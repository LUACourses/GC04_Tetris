### Description
Un clone de Tétris écrit en LUA et Love2D.  

===
### Objectifs
Empiler les pièces en bas de l'écran sans laisser d'espace vide.  
Chaque ligne complète disparaît et rapporte des points.  
Si les pièces empilées atteignent le haut de l'écran, la partie est terminée.  

### Mouvements et actions du joueur
Faire pivoter la pièce: touche Z ou flèche haut du clavier.  
Déplacer la pièce: touches Z et D ou bien les flèches droite et gauche du clavier.  
Faire descendre la pièce: touche S ou flèche bas du clavier.  
Mettre en pause : touche P.  
Relancer la partie: touche R.  
Musique précédente: touche F1.  
Musique suivante: touche F2.  
Quitter le jeu: clic sur la croix ou touche escape.  

#### en mode debug uniquement  
Activer les déplacements avec le clavier (ON/OFF): touche F10.  
Activer les déplacements avec la souris (ON/OFF): touche F11.  
Confiner la souris dans la fenêtre (ON/OFF): touche F12.  
Prochain Niveau: touche + (pavé numérique).  
Perdre une vie: touche - (pavé numérique).  
Perdre la partie: touche Fin.  
Gagner la partie: touche Debut.  

### Interactions
La majorité des règles du Tétris original sont appliquées.  

### Copyrights
Ecrit en LUA et en utilisant le Framework Love2D.  
Développé sans outil spécifique, en utilisant principalement SublimeText 3 et ZeroBrane Studio (pour le débuggage).  

Inspiré du cours dispensé par l'école de formation en ligne "GameCodeur", disponible sur le site http://www.gamecodeur.fr

-----------------
(C) 2017 GameCoderBlog (http://www.gamecoderblog.com)

=================================================

### Description
A Tetris clone written in LUA and Love2D.

===
### Goals
Stack the pieces at the bottom of the screen without leaving any empty space.  
Each complete line disappears and give points.  
If the stacked pieces reach the top of the screen, the game is over.  

### Movements and actions of the player
Rotate the piece: Z key or the up arrow on the keyboard.  
Move the piece: keys Z and D or the right and left arrows of the keyboard.  
Rotate the piece: S key or the down arrow on the keyboard.  
Pause: P key.  
Restart the game: R key.  
Previous music: F1 key.  
Next music: F2 key.  
Exit the game: click on the cross or escape key.  

### debug mode only
Enable the movements with the keyboard (ON/OFF): F10 key.  
Enable the movements with the mouse (ON/OFF): F11 key.  
Confining the mouse in the window (ON/OFF): F12 key.  
Next Level: + key (keypad).  
Lose a life: - key (keypad).  
Lose the game: End key.  
Win the game: Home key.  

### Interactions
The majority of the original Tetris rules are applied.

### Copyrights
Written in LUA and with the Love2D Framework.  
Developed without any specific tool, mainly using SublimeText 3 and ZeroBrane Studio (for debugging).  

Inspired by the course given by the online training school "GameCodeur", available on the site http://www.gamecodeur.fr

------------------
(C) 2017 GameCoderBlog (http://www.gamecoderblog.com)