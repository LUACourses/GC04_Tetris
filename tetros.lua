-- Objet représentant les tétrominos (pièces de tétris) pouvant être contrôlé par le joueur
-- Utilise une forme d'héritage simple: tetros qui hérite de actor qui hérite de object qui hérite de class
-- NOTE: contrairement à une héritage classique, les attributs de objets enfants sont COPIES depuis ceux du parent. 
-- donc CHAQUE CLASSE ENFANT DOIT UTILISER LES GETTERS/SETTERS DU PARENT POUR DÉFINIR LES ATTRIBUTS HÉRITÉS
--------------------------------
require("base/helpers")
require("base/actor")

function newTetros ()
  -- création d'un objet parent
  local super = newActor()
  -- création d'un objet enfant
  local self = super:new()

  -- attributs privés accessibles via les getter/setters
  -------------
  -- utilise les touches du clavier pour le déplacement de l'objet 
  local moveWithKeys
  -- utilise la souris pour le déplacement de l'objet 
  local moveWithMouse
  -- les touches utilisées pour les déplacements du joueur (propres à chaque joueur)
  local keys
  -- niveau
  local level
  -- couleur d'affichage
  local color
  -- chemin pour l'image représentant une vie
  local livesImagePath
  -- score
  local score
  -- nombre de vies au départ
  local livesStart
  -- nombre de vies restante
  local livesLeft
  -- le joueur a t'il gagné la partie ?
  local hasWon
  -- liste des touches de clavier devant être relâchées avant une action
  local keysToRelease = {}
  -- tétros en train de descendre
  local currentTetros = {}
  -- liste des couleurs associée à chaque forme
  local colors = {}
  -- grille d'affichage des tetros
  local grid = {}
  -- liste des formes possibles pour un tétro
  local shapes = {}
  -- liste des tetros devant être tirés au hasard
  -- NOTE: 
  -- Au départ la liste contient chaque forme 2 fois (ça permet de s'assurer que sur 14 tirages, chaque forme sortira 2 fois obligatoirement).
  -- Ensuite, à chaque tirage, on pioche dans le sac, et on supprime le tetros de la liste. Quand le sac est vide, on le remplie de nouveau.
  local bags = {}
  -- Etat du tétros
  local storeState = {x, y, shapeId, rotation}
  -- nombre de lignes complétées
  local linesCount

  --[[
  Initialise l'objet avec les valeurs données ou celles par défaut
    OPTIONNEL pWidth: largeur
    OPTIONNEL pHeight: hauteur
    OPTIONNEL pStartSpeed: vitesse de départ
    OPTIONNEL pVelocityMax: vélocité maximale. Si la valeur est nulle, l'objet ne pourra pas se déplacer
    OPTIONNEL pWeight: poids (influence la gravité et la résistance de l'environnement). Si la valeur est nulle, l'objet ne sera pas soumis à la gravité et au freinage 
    OPTIONNEL pLiveImagePath: chemin pour l'image représentant une vie. Mettre à "" pour ne pas utiliser d'image pour l'affichage des vies
    OPTIONNEL pLevel: niveau. Mettre à -1 pour ne pas afficher la valeur dans le HUD
    OPTIONNEL pLivesStart: nombre de vies restante.Mettre à -1 pour ne pas afficher la valeur dans le HUD
    OPTIONNEL pScore: score.Mettre à -1 pour ne pas afficher la valeur dans le HUD
    OPTIONNEL pMoveWithVelocity: true pour utiliser les vélocités pour le déplacement de l'objet (mouvement automatique)
    OPTIONNEL pMoveWithMouse: true pour utiliser la souris pour le déplacement de l'objet 
    OPTIONNEL pMoveWithKeys: true pour utiliser les touches du clavier pour le déplacement de l'objet 
  ]]

  function self.initialize (pWidth, pHeight, pStartSpeed, pVelocityMax, pWeight, pLiveImagePath, pLivesStart, pLevel, pScore, pMoveWithVelocity, pMoveWithKeys, pMoveWithMouse)
    debugFunctionEnter("tetros.initialize")
    assertEqualQuit(viewport, nil, "tetros.initialize:viewport", true)
    assertEqualQuit(gameSettings.playerKeys, nil, "tetros.initialize:gameSettings.playerKeys", true)

    if (pWidth == nil) then pWidth = 32 end
    if (pHeight == nil) then pHeight = 32 end
    if (pStartSpeed == nil) then pStartSpeed = 10 end
    if (pVelocityMax == nil) then pVelocityMax = 5 end
    if (pWeight == nil) then pWeight = 1 end
    if (pLiveImagePath == nil) then pLiveImagePath = "images/player_life.png" end
    if (pLivesStart == nil) then pLivesStart = 3 end
    if (pLevel == nil) then pLevel = 1 end 
    if (pScore == nil) then pScore = 0 end 
    if (pMoveWithVelocity == nil) then pMoveWithVelocity = true end 
    if (pMoveWithKeys == nil) then pMoveWithKeys = true end 
    if (pMoveWithMouse == nil) then pMoveWithMouse = true end 

    -- attributs de la classe parent
    -------------
    super.set_x(0)
    super.set_y(0)
    super.set_width(pWidth)
    super.set_height(pHeight)
    super.set_moveWithVelocity(pMoveWithVelocity) --NOTE. si true le mouvement utilisera l'inertie et le freinage, sinon les déplacements seront direct (déplacements standards)
    super.set_startSpeed(pStartSpeed)
    super.set_speed(super.get_startSpeed())
    super.set_velocityX(0)
    super.set_velocityY(0) 
    super.set_velocityMax(pVelocityMax)
    super.set_weight(pWeight) 
    super.set_gravityModularity(0)
    super.set_timerDrop(0)

    -- attributs spécifiques à cette classe
    -------------
    moveWithMouse = pMoveWithMouse
    moveWithKeys  = pMoveWithKeys

    -- par défaut on utilise les touches de clavier définies dans les réglages du jeu
    keys          = gameSettings.playerKeys
    level         = pLevel
    color         = {255, 46, 0} 
    livesImagePath = pLiveImagePath
    score         = pScore
    livesStart    = pLivesStart
    livesLeft     = pLivesStart
    hasWon        = false

    --Touches devant être relachées avant une action.
    keysToRelease[gameSettings.playerKeys.moveDown] = false
    keysToRelease[gameSettings.playerKeys.moveDownAlt] = false

    linesCount    = 0

    -- initialisation des tables de la classe
    -------------
    currentTetros   = {
      shapeId   = 1,
      rotation  = 1
    }
    
    grid            = {
      -- position X en pixel dans le viewport
      x = 0,
      -- largeur en nb de cellules
      width = 10,
      -- hauteur en nb de cellules
      height = 20,
      -- contenu (cellules)
      cells = {},
      -- compte du nombre de cellules pleines pour chaque ligne
      cellsCount = {},
      -- couleur de fond
      backgroundColor = {128, 128, 255, 50}
    }
    
    -- NOTE le nom et les couleurs des tetros ont été prise de la page https://fr.wikipedia.org/wiki/Tétromino
    -- I-Tetrimino  « bâton », « ordinaire », « barre », « long » Ligne de 4 carrées alignés.
    colors[1] = {0, 255, 255}
    shapes[1] = {
      {
        {0,0,0,0},
        {0,0,0,0},
        {1,1,1,1},
        {0,0,0,0},
      },
      {
        {0,1,0,0},
        {0,1,0,0},
        {0,1,0,0},
        {0,1,0,0},
      }
    }
    --O-Tetrimino « carré », « bloc » Méta-carré de 2x2.
    colors[2] = {255, 255, 0}
    shapes[2] = {
      {
        {0,0,0,0},
        {0,1,1,0},
        {0,1,1,0},
        {0,0,0,0},
      }
    }
    -- J-Tetrimino « L inversé », « gamma »  Trois carrés en ligne, avec un quatrième carré sous le côté droit.
    colors[3] = {0, 0, 255}
    shapes[3] = { 
      {
        {0,0,0},
        {1,1,1},
        {0,0,1},
      },
      {
        {0,1,0},
        {0,1,0},
        {1,1,0},
      },
      {
        {1,0,0},
        {1,1,1},
        {0,0,0},
      },
      {
        {0,1,1},
        {0,1,0},
        {0,1,0},
      } 
    }
    -- S-Tetrimino  « biais inversé » Méta-carré de 2x2, dont la rangée supérieure est glissée d'un pas vers la droite.
    colors[4] = {0, 255, 0}
    shapes[4] = {
      {
        {0,0,0},
        {0,1,1},
        {1,1,0},
      },
      {
        {0,1,0},
        {0,1,1},
        {0,0,1},
      },
      {
        {0,0,0},
        {0,1,1},
        {1,1,0},
      },
      {
        {0,1,0},
        {0,1,1},
        {0,0,1},
      } 
    }
    -- Z-Tetrimino « biais » Méta-carré de 2x2, dont la rangée supérieure est glissée d'un pas vers la gauche.
    colors[5] = {255, 0, 0}
    shapes[5] = {
      {
        {1,1,0},
        {0,1,1},
        {0,0,0},
      },
      {
        {0,1,0},
        {1,1,0},
        {1,0,0},
      }
    }
    --T-Tetrimino   Trois carrés en ligne, avec un quatrième carré sous le centre
    colors[6] = {170, 0, 255}
    shapes[6] = {
     {
        {0,0,0},
        {1,1,1},
        {0,1,0},
      },
      {
        {0,1,0},
        {1,1,0},
        {0,1,0},
      },
      {
        {0,1,0},
        {1,1,1},
        {0,0,0},
      },
      {
        {0,1,0},
        {0,1,1},
        {0,1,0},
      } 
    }
    -- L-Tetrimino   Trois carrés en ligne, avec un quatrième carré sous le côté gauche.
    colors[7] = {255, 165, 0}
    shapes[7] = { 
      {
        {0,0,0},
        {1,1,1},
        {1,0,0},
      },
      {
        {1,1,0},
        {0,1,0},
        {0,1,0},
      },
      {
        {0,0,1},
        {1,1,1},
        {0,0,0},
      },
      {
        {0,1,0},
        {0,1,0},
        {0,1,1},
      } 
    }
    
    self.initBag()
  end
  
  -- getters et setters pour les attributs de cet objet
  --------------------------------
  function self.get_moveWithKeys ()
    return moveWithKeys
  end
  function self.set_moveWithKeys (pValue)
    moveWithKeys = pValue 
  end
  function self.get_moveWithMouse ()
    return moveWithMouse
  end
  function self.set_moveWithMouse (pValue)
    moveWithMouse = pValue 
  end
  function self.get_keys ()
    return keys
  end
  function self.set_keys (pValue)
    keys = pValue 
  end
  function self.get_currentTetros ()
    return currentTetros
  end
  function self.set_currentTetros (pValue)
    currentTetros = pValue 
  end
  function self.get_colors ()
    return colors
  end
  function self.set_colors (pValue)
    colors = pValue 
  end
  function self.get_shapes ()
    return shapes
  end
  function self.set_shapes (pValue)
    shapes = pValue 
  end
  function self.get_grid ()
    return grid
  end
  function self.set_grid (pValue)
    grid = pValue 
  end
  -- Retourne l'id de la forme du tetros courant
  function self.get_currentShapeId ()
    return currentTetros.shapeId
  end
  -- Définit l'id de la forme du tetros courant
  function self.set_currentShapeId (pValue)
    currentTetros.shapeId = pValue 
  end
  -- Retourne l'index de la rotation du tetros courant
  function self.get_currentRotation ()
    return currentTetros.rotation
  end
  -- Définit l'index de la rotation du tetros courant
  function self.set_currentRotation (pValue)
    currentTetros.rotation = pValue 
  end
  -- Retourne la forme du tetros courant
  function self.get_currentShape ()
    return shapes[self.get_currentShapeId()][self.get_currentRotation()]
  end
  -- Retourne la couleur du tetros courant
  function self.get_currentColor ()
    return colors[self.get_currentShapeId()]
  end
  function self.get_linesCount ()
    return linesCount
  end
  function self.get_bag ()
    return bag
  end
  function self.set_bag (pValue)
    bag = pValue
  end
  function self.get_keysToRelease ()
    return keysToRelease
  end
  function self.set_keysToRelease (pValue)
    keysToRelease = pValue 
  end
  function self.get_keyToRelease (pIndex)
    return keysToRelease[pIndex]
  end
  function self.set_keyToRelease (pIndex, pValue)
    keysToRelease[pIndex] = pValue 
  end

  -- override des fonctions des parents
  --------------------------------
  -- Détruit l'objet (override)
  function self.destroy ()
    debugFunctionEnter("tetros.destroy")
    self.clean()
    self = nil
  end

  -- Effectue du nettoyage lié à l'objet en quittant le jeu (override)
  function self.clean ()
    debugFunctionEnter("tetros.clean")
  end

  --[[
  Actualise l'objet (override)
    pDt: delta time
  ]]
  function self.update (pDt)
    --debugFunctionEnter("tetros.update") --ATTENTION cet appel peut remplir le log
    -- pour le moment rien à faire
    super.update(pDt)
  end
  
  -- Dessine l'objet (override)
  function self.draw ()
    self.drawShape()
  end

  --[[
  Déplace l'objet (override)
    pDt: delta time
  ]]
  function self.move (pDt) 
    --debugFunctionEnter("tetros.move") --ATTENTION cet appel peut remplir le log
    super.move(pDt)
  end

  --[[
  Vérifie les collisions et adapte le mouvement en conséquence (override)
    OPTIONNEL pCollisionXmin: dimension X minimale à vérifier
    OPTIONNEL pCollisionXmax: dimension X maximale à vérifier
    OPTIONNEL pCollisionYmin: dimension Y minimale à vérifier
    OPTIONNEL pCollisionYmax: dimension Y maximale à vérifier
		RETURN : 1 en cas de collision avec les bords, 2 en cas de collision avec une pièce ou 0 sinon
  ]]
  function self.collide (pCollisionXmin, pCollisionXmax, pCollisionYmin, pCollisionYmax) 
    --debugFunctionEnter("tetros.collide:", pCollisionXmin, pCollisionXmax, pCollisionYmin, pCollisionYmax) --ATTENTION cet appel peut remplir le log
    assertEqualQuit(viewport, nil, "tetros.collide:viewport", true)
    local l, c, x ,y
    
    -- conversion des coordonnées pixel vers des coordonnées grille
    x = math.floor((super.get_x() / super.get_width())) + 1
    y = math.floor((super.get_y() / super.get_height())) + 1

    local shape = self.get_currentShape()
    local grid = self.get_grid()

    for l = 1, #shape do
      for c = 1, #shape[l] do
        local cInGrid = (c - 1) + x 
        local lInGrid = (l - 1) + y
        if (lInGrid <= 0) then lInGrid = 1 end
        --debugMessage("x,y  ", x, y)
        --debugMessage("cInGrid,lInGrid  ", cInGrid, lInGrid)
        if (shape[l][c] ~= 0) then
          if (cInGrid <= 0 or cInGrid > grid.width) then
            -- collision avec les bords droit et gauche de la grille
            return 1
          end
          if (lInGrid > grid.height) then
            -- dépasse la ligne du bas de la grille
           return 2
          end
          if (grid.cells[lInGrid][cInGrid] ~= 0) then
            -- collision avec une case déjà occupée par une partie d'un autre tetros
            return 2
          end
        end
        --debugMessage("cInGrid,lInGrid  ", cInGrid, lInGrid, "=", grid.cells[lInGrid][cInGrid])
      end
    end
    return false
  end

  -- place l'objet dans la zone de jeu (override)
  function self.spawn ()
    debugFunctionEnter("tetros.spawn")
    assertEqualQuit(viewport, nil, "tetros.spawn:viewport", true)
    local shapeId = self.getInBag()
    --debugMessage("tetros.spawn shapeId ",shapeId)
    self.set_currentShapeId(shapeId)
    self.set_currentRotation(1)
    
    local shape = self.get_currentShape()
    local tetrosWidth = #shape[1]
    local tetrosX = math.floor((self.get_grid().width - tetrosWidth) / 2) + 1
    super.set_x(viewport.Xmin + tetrosX * super.get_width()) -- au milieu de la grille
    super.set_y(viewport.Ymin) -- en haut de la grille
    -- le tetros descend par étapes correspondant à la hauteur d'une ligne
    super.set_gravityModularity(super.get_height())
    super.set_timerDrop(0)
  end  
  
  -- fonctions spécifiques
  --------------------------------
  --[[
  Interactions de l'objet avec le clavier
    pKey: Caractère de la touche pressée.
    OPTIONNEL pScancode: le scancode représentant la touche pressée.
    OPTIONNEL pIsrepeat: TRUE si cet événement keypress est une répétition. Le délai entre les répétitions des touches dépend des paramètres système de l'utilisateur.
  ]]
  function self.keypressed (pKey, pScancode, pIsrepeat)
    --debugFunctionEnter("tetros.keyPressed:", pKey, pScancode, pIsrepeat)

    -- Activer les déplacements avec le clavier (ON/OFF).
    if (DEBUG_MODE and pKey == gameSettings.gameKeys.moveWithKeys) then 
      local state = not self.get_moveWithKeys()
      debugMessage("tetros.moveWithKeys=", state) 
      self.set_moveWithKeys(state)
    end
    -- Activer les déplacements avec la souris (ON/OFF).
    if (DEBUG_MODE and pKey == gameSettings.gameKeys.moveWithMouse) then 
      local state = not self.get_moveWithMouse()
      debugMessage("tetros.moveWithMouse=", state) 
      self.set_moveWithMouse(state)
    end

    self.store()

    -- déplacement avec le clavier
    if (gameState.get_currentState() == SCREEN_PLAY) then
      if (self.get_moveWithKeys()) then
        -- rotation du tétro courant
        if (pKey == self.get_keys().moveUp or pKey == self.get_keys().moveUpAlt) then
          self.set_currentRotation(self.get_currentRotation() + 1)
          if (self.get_currentRotation() > #self.get_shapes()[self.get_currentShapeId()]) then self.set_currentRotation(1) end  
        end 
        if (pKey == self.get_keys().moveDown or pKey == self.get_keys().moveDownAlt) then
          self.set_velocityY(self.get_velocityY() + self.get_speed())
        end 
        if (pKey == self.get_keys().moveLeft or pKey == self.get_keys().moveLeftAlt) then
          self.set_velocityX(self.get_velocityX() - self.get_speed()) 
        end 
        if (pKey == self.get_keys().moveRight or pKey == self.get_keys().moveRightAlt) then
          self.set_velocityX(self.get_velocityX() + self.get_speed()) 
        end 
      end
    end --if (gameState.get_currentState() == SCREEN_PLAY) then   
  end

  --[[
  Interactions de l'objet avec la souris
    OPTIONNEL pX: la position de la souris sur l'axe des x.
    OPTIONNEL pY: la position de la souris sur l'axe des y.
    OPTIONNEL pDX: La quantité a été déplacée le long de l'axe des abscisses depuis la dernière fois que la fonction a été appelée.
    OPTIONNEL pDY: La quantité a évolué le long de l'axe des y depuis la dernière fois que la fonction a été appelée.
    OPTIONNEL pIstouch: Vrai si le bouton de la souris appuie sur la touche à partir d'une touche tactile à l'écran tactile.
  ]]
  function self.mousemoved (pX, pY, pDX, pDY, pIstouch)
    --debugFunctionEnter("tetros.mousemoved", pX, pY, pDX, pDY, pIstouch)  --ATTENTION cet appel peut remplir le log
    if (gameState.get_currentState() == SCREEN_PLAY) then   
      if (self.get_moveWithMouse()) then 
        -- déplacements avec la souris
        --super.set_x (pX - super.get_width() / 2)
        --super.set_y (pY - super.get_height() / 2)
        self.set_velocityX(self.get_velocityX() + self.get_speed() * gameSettings.mouse.sensibilityX * pDX) 
        self.set_velocityY(self.get_velocityY() + self.get_speed() * gameSettings.mouse.sensibilityY * pDY)
        --debugMessage("pDX,pDY=", pDX ,pDY, "VX,VY=", self.get_velocityX() ,self.get_velocityY())
      end
    end --if (gameState.get_currentState() == SCREEN_PLAY) then 
  end

  --[[
  intercepte le clic de souris
    pX: position x de la souris, en pixels.
    pY: position y de la souris, en pixels.
    pButton: L'index du bouton qui a été pressé: 1 est le bouton principal, 2 est le bouton secondaire et 3 est le bouton du milieu.
    OPTIONNEL pIstouch: Vrai si le bouton est une touche tactile.
  ]]
  function self.mousepressed (pX, pY, pButton, pIstouch)
    --debugFunctionEnter("tetros.mousepressed:", pX, pY, pButton, pIstouch)
            
    self.store()
    
    if (gameState.get_currentState() == SCREEN_PLAY) then 
      -- action possible avec le bouton de la souris si le jeu est en cours
      if (DEBUG_MODE) then
        -- debug only, on change le tétro courant
        self.set_currentShapeId(self.get_currentShapeId() + 1)
        self.set_currentRotation(1)
        if (self.get_currentShapeId() > #self.get_shapes()) then
          self.set_currentShapeId(1)
        end
      end
    end --if (gameState.get_currentState() == SCREEN_PLAY) then 
  end

  --[[
  Dessine la forme de tetros donnée
    OPTIONNEL pShapeId: Index de la forme à dessiner. Si absent utilise celle la forme en cours
    OPTIONNEL pRotation: Rotation de la forme à dessiner. Si absent utilise celle la forme en cours
    OPTIONNEL pX: positon X de l'objet (en pixel). Si absent utilise la position X du tetros courant
    OPTIONNEL pY: positon Y de l'objet (en pixel). Si absent utilise la position Y du tetros courant
    OPTIONNEL pScale: facteur d'échelle pour le dessin. Si absent, vaut 1
  ]]
  function self.drawShape (pShapeId, pRotation, pX, pY, pScale)
    --debugFunctionEnter("tetros.drawShape", pShapeId, pRotation, pX, pY) --ATTENTION cet appel peut remplir le log
    assertEqualQuit(viewport, nil, "tetros.drawShape:viewport", true)

    if (pShapeId == nil) then pShapeId = self.get_currentShapeId() end
    if (pRotation == nil) then pRotation = self.get_currentRotation() end
    if (pX == nil) then pX = super.get_x() end
    if (pY == nil) then pY = super.get_y() end
    if (pScale == nil) then pScale = 1 end

    local shape = shapes[pShapeId][pRotation]
    local color = colors[pShapeId]
    local grid = self.get_grid()
    local x, y, r, c
    local cellWidth = super.get_width() * pScale
    local cellHeight = super.get_height() * pScale

    for r = 1, #shape do
      for c = 1, #shape[r] do
       -- conversion des coordonnées grille vers des coordonnées pixel      
        x = pX + (c - 1) * cellWidth
        y = pY + (r - 1) * cellHeight
        -- ajout de la position X de la grille
        x = x + self.get_grid().x
        if (shape[r][c] ~= 0) then 
          love.graphics.setColor (color[1], color[2], color[3])
          love.graphics.rectangle("fill", x, y, cellWidth - 1, cellHeight - 1)
        end
      end
    end 
  end

  -- initialise la grille de jeu
  function self.initGrid ()
    --debugFunctionEnter("tetros.initGrid")
    local grid = self.get_grid()
    local c, l 
    local cellSize = math.floor((viewport.Ymax - viewport.Ymin) / grid.height) 
    super.set_width (cellSize)
    super.set_height (cellSize) 
    -- les 3 lignes ci-dessous permettent d'imposer un déplacement par pas correspondant à une case de la grille
    super.set_speed (cellSize)
    super.set_startSpeed (cellSize)
    super.set_velocityMax (cellSize)

    grid.x = (viewport.Xmax - viewport.Xmin) / 2 - (cellSize * grid.width) / 2
    grid.cells = {} 
    for l = 1, grid.height do 
      grid.cells[l] = {}
      grid.cellsCount[l] = 0
      for c = 1, grid.width do
        grid.cells[l][c] = 0
      end
    end
  end

  -- dessine la grille de jeu
  function self.drawGrid ()
    --debugFunctionEnter("tetros.drawGrid") 
    local grid = self.get_grid()
    local c, l, x, y 
    local w = super.get_width()
    local h = super.get_height()

    for l = 1, grid.height do 
      for c = 1, grid.width do
        x = (c - 1) * w + grid.x 
        y = (l - 1) * h
        if ( grid.cells[l][c] == 0) then 
          love.graphics.setColor(grid.backgroundColor[1], grid.backgroundColor[2], grid.backgroundColor[3], grid.backgroundColor[4]) 
        else
          local color = self.get_colors()[grid.cells[l][c]]
          love.graphics.setColor(color[1], color[2], color[3]) 
        end
        love.graphics.rectangle("fill", x, y, w - 1, h - 1)
      end
    end
  end
  
  -- mémorise l'état du tetros courant
  function self.store ()
    --debugFunctionEnter("tetros.store") --ATTENTION cet appel peut remplir le log
    storeState.x = super.get_x()
    storeState.y = super.get_y()
    storeState.shapeId = self.get_currentShapeId()
    storeState.rotation = self.get_currentRotation()
  end
  
  -- restore l'état du tetros courant
  function self.restore ()
     --debugFunctionEnter("tetros.restore")
    super.set_x(storeState.x)
    super.set_y(storeState.y)
    self.set_currentShapeId(storeState.shapeId)
    self.set_currentRotation(storeState.rotation)
  end
  
  -- transfère le contenu du tetros courant dans la grille 
  function self.transfer () 
    --debugFunctionEnter("tetros.transfer")
    local l, c, x ,y
    -- conversion des coordonnées pixel vers des coordonnées grille
    x = math.floor((super.get_x() / super.get_width())) + 1
    y = math.floor((super.get_y() / super.get_height())) + 1
    y = y - 1
    --debugMessage("x,y  ", x, y)
    
    local shape = self.get_currentShape()
    local grid = self.get_grid()

    for l = 1, #shape do
      for c = 1, #shape[l] do
        local cInGrid = (c - 1) + x 
        local lInGrid = (l - 1) + y
        if (cInGrid > grid.width) then cInGrid = grid.width end
        if (lInGrid > grid.height) then lInGrid = grid.height end
        if (shape[l][c] ~= 0) then 
          grid.cells[lInGrid][cInGrid] = self.get_currentShapeId() 
          grid.cellsCount[lInGrid] = grid.cellsCount[lInGrid] + 1 
        end
      end
    end

    -- on vérifie et élimine les lignes completes
    local count = self.checkFullLines() 
    local score 
    if (count > 0) then 
      -- au moins une ligne complete
      score = (100 + math.pow(2, count) * 100) * player.get_level()
      linesCount = linesCount + count
      local level = math.floor(linesCount / 10) + 1
      if (player.get_level() ~= level) then 
        nextLevel(level) 
      end
    else 
      score = 4 * player.get_level()
    end 
    debugMessage("Score:", score)
    player.addScore(score)

    -- on spawne un autre tetros
    self.spawn()
    if (self.collide()) then 
      -- s'il collisionne, on est en haut de la grille et la partie est perdue
      loseGame() 
    else
      player.addScore(4 * player.get_level())
    end

    --Touches devant être relachées avant une action.
    keysToRelease[gameSettings.playerKeys.moveDown] = true
    keysToRelease[gameSettings.playerKeys.moveDownAlt] = true
  end
  
  --[[
  supprime une ligne de la grille est fait descendre les lignes précédentes
    pLine: index de la ligne à supprimer
  ]]
  function self.removeLine (pLine) 
    --debugFunctionEnter("tetros.removeLine:", pLine)    
    local grid = self.get_grid()
    if (pLine > grid.height or pLine < 2 ) then return end
    local l, c
    for l = pLine, 2, -1 do
      for c = 1, grid.width do
        grid.cells[l][c] = grid.cells[l - 1][c]
      end
      grid.cellsCount[l] = grid.cellsCount[l - 1]
    end

    sndLineComplete:play()
  end
  
  --[[
  Vérifie et supprime les lignes complètes
  RETURN: le nombre de lignes supprimées
  ]]
  function self.checkFullLines () 
    --debugFunctionEnter("tetros.checkFullLines")    
    local grid = self.get_grid()
    local l
    local count = 0
    for l = 1, grid.height do
      -- on supprime la ligne de la grille si elle est pleine
      if (grid.cellsCount[l] >= grid.width) then 
        self.removeLine(l)
        count = count + 1
      end
    end
    return count
  end
  
  -- Initialise la liste des tétros à tirer
  function self.initBag()
  --debugFunctionEnter("tetros.initBag")    
    self.set_bag({})
    -- chaque forme est ajoutée 3 fois dans la liste
    for n = 1, #self.get_shapes() do
      table.insert(self.get_bag(), n)
      table.insert(self.get_bag(), n)
      table.insert(self.get_bag(), n)
    end
    -- on mélange la liste
    self.set_bag(shuffle(self.get_bag()))
  end
  
  -- pioche un tétros dans la liste des tétros à tirer
  function self.getInBag()
    --debugFunctionEnter("tetros.getInBag")    
    local bagIndex = 1 -- on prend le 1er élément, la liste étant déjà mélangée
    local shapeId = self.get_bag()[bagIndex]
    table.remove(self.get_bag(), bagIndex)
    if (#self.get_bag() == 0) then
      self.initBag()
    end
    return shapeId
  end
  
  -- affiche la pile à l'écran
  function self.drawBag() 
    --debugFunctionEnter("tetros.drawBag") --ATTENTION cet appel peut remplir le log
    local i, shapeId, cellHeight, cellWidth, shape
    local scale = 0.4
    local drawX = (viewport.Xmax - viewport.Xmin) - super:get_width() * 13
    print(drawX)
    local drawY = 50
    local max = 10
    -- cas ou il reste moins de 10 tetros dans la pile
    if (max > #self.get_bag()) then max = #self.get_bag() end
    -- affiche les 10 prochains tétros
    for i = 1, max do
      shapeId = self.get_bag()[i]
      shape = self.get_shapes()[shapeId]
      cellWidth = #shape[1] * self.get_width() * scale 
      cellHeight = #shape[1] * self.get_height() * scale 

      rectX = drawX + self.get_grid().x -1
      rectY = drawY - 1
      rectWidth = 4 * self.get_width() * scale 
      rectHeight = 4 * self.get_height() * scale 
      
      self.drawShape(shapeId, 1, drawX + (rectWidth - cellWidth) / 2, drawY + (rectHeight - cellHeight) / 2, scale)
      
      love.graphics.setColor (255, 255, 255, 255)
      love.graphics.rectangle("line", rectX , rectY, rectWidth + 1, rectHeight + 1)

      drawY = drawY + rectHeight + 5
    end
  end 

  -- initialisation par défaut
  self.initialize()
  return self
end --function