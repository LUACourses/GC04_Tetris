-- Regroupe les constantes de paramétrage du jeu
-- Elles ne devraient pas être modifiées pendant l'éxécution
--------------------------------

--[[
Définit des niveau d'affichage des information de debug (affiche des infos supplémentaires)
	0: aucun
	1: messages affichés via la fonction debugMessage
  2: comme précédent mais affiche aussi les infos de debug dans le HUD (variable debugInfos)
  3: comme précédent mais affiche aussi les messages affichés via la fonction debugFunctionEnter 
]] 
DEBUG_MODE = 3

-- Titre
APP_TITLE = "Tetris" 

-- Chemin de l'image utilisée comme icône de l'application
APP_ICON = "images/icon.png"

-- Largeur de la fenêtre de l'application
APP_WINDOWS_WIDTH = 800 

-- Hauteur de la fenêtre de l'application
APP_WINDOWS_HEIGHT = 600

-- nom des différents écrans de l'application
SCREEN_PLAY  = "screen_play"
SCREEN_PAUSE = "screen_pause"
SCREEN_START = "screen_start"
SCREEN_END   = "screen_end"
