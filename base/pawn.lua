-- Objet représentant un objet mobile pouvant être contrôlé par le joueur
-- Utilise une forme d'héritage simple: pawn qui hérite de actor qui hérite de object qui hérite de class
-- NOTE: contrairement à une héritage classique, les attributs de objets enfants sont COPIES depuis ceux du parent. 
-- donc CHAQUE CLASSE ENFANT DOIT UTILISER LES GETTERS/SETTERS DU PARENT POUR DÉFINIR LES ATTRIBUTS HÉRITÉS
--------------------------------
require("base/helpers")
require("base/actor")

function newPawn ()
  -- création d'un objet parent
  local super = newActor()
  -- création d'un objet enfant
  local self = super:new()

  -- attributs privés accessibles via les getter/setters
  -------------
  -- utilise les touches du clavier pour le déplacement de l'objet 
  local moveWithKeys
  -- utilise la souris pour le déplacement de l'objet 
  local moveWithMouse
  -- les touches utilisées pour les déplacements du joueur (propres à chaque joueur)
  local keys
  -- niveau
  local level
  -- couleur d'affichage
  local color
  -- chemin pour l'image représentant une vie
  local livesImagePath
  -- score
  local score
  -- nombre de vies au départ
  local livesStart
  -- nombre de vies restante
  local livesLeft
  -- le joueur a t'il gagné la partie ?
  local hasWon
  -- liste des touches de clavier devant être relâchées avant une action
  local keysToRelease = {}

  --[[
  Initialise l'objet avec les valeurs données ou celles par défaut
    OPTIONNEL pWidth: largeur
    OPTIONNEL pHeight: hauteur
    OPTIONNEL pStartSpeed: vitesse de départ
    OPTIONNEL pVelocityMax: vélocité maximale. Si la valeur est nulle, l'objet ne pourra pas se déplacer
    OPTIONNEL pWeight: poids (influence la gravité et la résistance de l'environnement). Si la valeur est nulle, l'objet ne sera pas soumis à la gravité et au freinage 
    OPTIONNEL pLiveImagePath: chemin pour l'image représentant une vie. Mettre à "" pour ne pas utiliser d'image pour l'affichage des vies
    OPTIONNEL pLevel: niveau. Mettre à -1 pour ne pas afficher la valeur dans le HUD
    OPTIONNEL pLivesStart: nombre de vies restante.Mettre à -1 pour ne pas afficher la valeur dans le HUD
    OPTIONNEL pScore: score.Mettre à -1 pour ne pas afficher la valeur dans le HUD
    OPTIONNEL pMoveWithVelocity: true pour utiliser les vélocités pour le déplacement de l'objet (mouvement automatique)
    OPTIONNEL pMoveWithMouse: true pour utiliser la souris pour le déplacement de l'objet 
    OPTIONNEL pMoveWithKeys: true pour utiliser les touches du clavier pour le déplacement de l'objet 
  ]]

  function self.initialize (pWidth, pHeight, pStartSpeed, pVelocityMax, pWeight, pLiveImagePath, pLivesStart, pLevel, pScore, pMoveWithVelocity, pMoveWithKeys, pMoveWithMouse)
    debugFunctionEnter("pawn.initialize")
    assertEqualQuit(viewport, nil, "pawn.initialize:viewport", true)
    assertEqualQuit(gameSettings.playerKeys, nil, "pawn.initialize:gameSettings.playerKeys", true)

    if (pWidth == nil) then pWidth = 32 end
    if (pHeight == nil) then pHeight = 32 end
    if (pStartSpeed == nil) then pStartSpeed = 10 end
    if (pVelocityMax == nil) then pVelocityMax = 5 end
    if (pWeight == nil) then pWeight = 1 end
    if (pLiveImagePath == nil) then pLiveImagePath = "images/player_life.png" end
    if (pLivesStart == nil) then pLivesStart = 3 end
    if (pLevel == nil) then pLevel = 1 end 
    if (pScore == nil) then pScore = 0 end 
    if (pMoveWithVelocity == nil) then pMoveWithVelocity = true end 
    if (pMoveWithKeys == nil) then pMoveWithKeys = true end 
    if (pMoveWithMouse == nil) then pMoveWithMouse = true end 

    -- attributs de la classe parent
    -------------
    super.set_x(0)
    super.set_y(0)
    super.set_width(pWidth)
    super.set_height(pHeight)
    super.set_moveWithVelocity(pMoveWithVelocity) --NOTE. si true le mouvement utilisera l'inertie et le freinage, sinon les déplacements seront direct (déplacements standards)
    super.set_startSpeed(pStartSpeed)
    super.set_speed(super.get_startSpeed())
    super.set_velocityX(0)
    super.set_velocityY(0) 
    super.set_velocityMax(pVelocityMax)
    super.set_weight(pWeight) 
    super.set_gravityModularity(0)
    super.set_timerDrop(0)

    -- attributs spécifiques à cette classe
    -------------
    moveWithMouse = pMoveWithMouse
    moveWithKeys  = pMoveWithKeys

    -- par défaut on utilise les touches de clavier définies dans les réglages du jeu
    keys          = gameSettings.playerKeys
    level         = pLevel
    color         = {255, 46, 0} 
    livesImagePath = pLiveImagePath
    score         = pScore
    livesStart    = pLivesStart
    livesLeft     = pLivesStart
    hasWon        = false

    --Touches devant être relachées avant une action.
    --keysToRelease[gameSettings.playerKeys.moveDown] = false
    --keysToRelease[gameSettings.playerKeys.moveDownAlt] = false
  end 

  -- getters et setters pour les attributs de cet objet
  --------------------------------
  function self.get_moveWithKeys ()
    return moveWithKeys
  end
  function self.set_moveWithKeys (pValue)
    moveWithKeys = pValue 
  end
  function self.get_moveWithMouse ()
    return moveWithMouse
  end
  function self.set_moveWithMouse (pValue)
    moveWithMouse = pValue 
  end
  function self.get_keys ()
    return keys
  end
  function self.set_keys (pValue)
    keys = pValue 
  end
  function self.get_level ()
    return level
  end
  function self.set_level (pValue)
    level = pValue 
  end
  function self.get_color ()
    return color
  end
  function self.set_color (pValue)
    color = pValue 
  end
  function self.get_livesImagePath ()
    return livesImagePath
  end
  function self.set_livesImagePath (pValue)
    livesImagePath = pValue 
  end
  function self.get_score ()
    return score
  end
  function self.set_score (pValue)
    score = pValue 
  end
  function self.get_livesLeft ()
    return livesLeft
  end
  function self.set_livesLeft (pValue)
    livesLeft = pValue 
  end
  function self.get_livesStart ()
    return livesStart
  end
  function self.set_livesStart (pValue)
    livesStart = pValue 
  end
  function self.get_hasWon ()
    return hasWon
  end
  function self.set_hasWon (pValue)
    hasWon = pValue 
  end
  function self.get_keys ()
    return keys
  end
  function self.set_keys (pValue)
    keys = pValue 
  end
  function self.get_keysToRelease ()
    return keysToRelease
  end
  function self.set_keysToRelease (pValue)
    keysToRelease = pValue 
  end
  function self.get_keyToRelease (pIndex)
    return keysToRelease[pIndex]
  end
  function self.set_keyToRelease (pIndex, pValue)
    keysToRelease[pIndex] = pValue 
  end

  -- override des fonctions des parents
  --------------------------------
  -- Détruit l'objet (override)
  function self.destroy ()
    debugFunctionEnter("pawn.destroy")
    self.clean()
    self = nil
  end

  -- Effectue du nettoyage lié à l'objet en quittant le jeu (override)
  function self.clean ()
    debugFunctionEnter("pawn.clean")
    local backgroundColor
    
    if (assertEqual(viewport.backgroundColor, nil, "pawn.destroy:backgroundColor")) then 
      backgroundColor = {0, 0, 0} 
    else 
      backgroundColor = viewport.backgroundColor
    end

    -- efface l'objet
    love.graphics.setColor(backgroundColor[1], backgroundColor[2], backgroundColor[3])
    love.graphics.rectangle("fill", super.get_x(), super.get_y(), super.get_width(), super.get_height())    
  end

  --[[
  Actualise l'objet (override)
    pDt: delta time
  ]]
  function self.update (pDt)
    --debugFunctionEnter("pawn.update") --ATTENTION cet appel peut remplir le log
    -- pour le moment rien à faire
    super.update(pDt)
  end

  -- Dessine l'objet (override)
  function self.draw ()
    --debugFunctionEnter("pawn.draw") --ATTENTION cet appel peut remplir le log
    love.graphics.setColor(self.get_color(1), self.get_color(2), self.get_color(3))
    love.graphics.rectangle("fill", super.get_x(), super.get_y(), super.get_width(), super.get_height(), 5, 5)
  end

  --[[
  Déplace l'objet (override)
    pDt: delta time
  ]]
  function self.move (pDt) 
    --debugFunctionEnter("pawn.move:", pDt) --ATTENTION cet appel peut remplir le log
    super.move(pDt)
  end

  --[[
  Vérifie les collisions et adapte le mouvement en conséquence (override)
    OPTIONNEL pCollisionXmin: dimension X minimale à vérifier
    OPTIONNEL pCollisionXmax: dimension X maximale à vérifier
    OPTIONNEL pCollisionYmin: dimension Y minimale à vérifier
    OPTIONNEL pCollisionYmax: dimension Y maximale à vérifier
    RETURN : true si il y a une collision ou false sinon
    NOTE: en cas de collision, rectifie les attributs x et y de l'objet
  ]]
  function self.collide (pCollisionXmin, pCollisionXmax, pCollisionYmin, pCollisionYmax) 
    --debugFunctionEnter("pawn.collide:", pCollisionXmin, pCollisionXmax, pCollisionYmin, pCollisionYmax) --ATTENTION cet appel peut remplir le log
    assertEqualQuit(viewport, nil, "pawn.collide:viewport", true)
    if (pCollisionYmax == nil) then 
      pCollisionYmax = viewport.Ymax - super.get_height()
      --le joueur ne peut pas aller sur les 2 dernières lignes, réservées à l'affichage 
      pCollisionYmax = pCollisionYmax - viewport.charHeight * 2 
    end 
    return super.collide(pCollisionXmin, pCollisionXmax, pCollisionYmin, pCollisionYmax)
  end

  -- place l'objet dans la zone de jeu (override)
  function self.spawn ()
    debugFunctionEnter("pawn.spawn")
    assertEqualQuit(viewport, nil, "pawn.spawn:viewport", true)
    super.set_x(((viewport.Xmax - viewport.Xmin) - super.get_width()) / 2)
    super.set_y(((viewport.Ymax - viewport.Ymin) - super.get_height() - viewport.charHeight * 2) / 2)
    super.set_timerDrop(0)
  end  
  
  -- fonctions spécifiques
  --------------------------------
  --[[
  Interactions de l'objet avec le clavier
    pKey: Caractère de la touche pressée.
    OPTIONNEL pScancode: le scancode représentant la touche pressée.
    OPTIONNEL pIsrepeat: TRUE si cet événement keypress est une répétition. Le délai entre les répétitions des touches dépend des paramètres système de l'utilisateur.
  ]]
  function self.keypressed (pKey, pScancode, pIsrepeat)
    --debugFunctionEnter("pawn.keyPressed:", pKey, pScancode, pIsrepeat)

    -- Activer les déplacements avec le clavier (ON/OFF).
    if (DEBUG_MODE and pKey == gameSettings.gameKeys.moveWithKeys) then 
      local state = not self.get_moveWithKeys()
      debugMessage("tetros.moveWithKeys=", state) 
      self.set_moveWithKeys(state)
    end
    -- Activer les déplacements avec la souris (ON/OFF).
    if (DEBUG_MODE and pKey == gameSettings.gameKeys.moveWithMouse) then 
      local state = not self.get_moveWithMouse()
      debugMessage("tetros.moveWithMouse=", state) 
      self.set_moveWithMouse(state)
    end

    -- déplacement avec le clavier
    if (gameState.get_currentState() == SCREEN_PLAY) then
      if (self.get_moveWithKeys()) then
        if (pKey == self.get_keys().moveUp or pKey == self.get_keys().moveUpAlt) then
          self.set_velocityY(self.get_velocityY() - self.get_speed())
        end 
        if (pKey == self.get_keys().moveDown or pKey == self.get_keys().moveDownAlt) then
          self.set_velocityY(self.get_velocityY() + self.get_speed())
        end 
        if (pKey == self.get_keys().moveLeft or pKey == self.get_keys().moveLeftAlt) then
          self.set_velocityX(self.get_velocityX() - self.get_speed()) 
        end 
        if (pKey == self.get_keys().moveRight or pKey == self.get_keys().moveRightAlt) then
          self.set_velocityX(self.get_velocityX() + self.get_speed()) 
        end 
      end
    end --if (gameState.get_currentState() == SCREEN_PLAY) then   
  end

  --[[
  Interactions de l'objet avec la souris
    OPTIONNEL pX: la position de la souris sur l'axe des x.
    OPTIONNEL pY: la position de la souris sur l'axe des y.
    OPTIONNEL pDX: La quantité a été déplacée le long de l'axe des abscisses depuis la dernière fois que la fonction a été appelée.
    OPTIONNEL pDY: La quantité a évolué le long de l'axe des y depuis la dernière fois que la fonction a été appelée.
    OPTIONNEL pIstouch: Vrai si le bouton de la souris appuie sur la touche à partir d'une touche tactile à l'écran tactile.
  ]]
  function self.mousemoved (pX, pY, pDX, pDY, pIstouch)
    --debugFunctionEnter("pawn.mousemoved", pX, pY, pDX, pDY, pIstouch)  --ATTENTION cet appel peut remplir le log
    if (gameState.get_currentState() == SCREEN_PLAY) then   
      if (self.get_moveWithMouse()) then 
        -- déplacements avec la souris
        --super.set_x (pX - super.get_width() / 2)
        --super.set_y (pY - super.get_height() / 2)
        self.set_velocityX(self.get_velocityX() + self.get_speed() * gameSettings.mouse.sensibilityX * pDX) 
        self.set_velocityY(self.get_velocityY() + self.get_speed() * gameSettings.mouse.sensibilityY * pDY)
        --debugMessage("pDX,pDY=", pDX ,pDY, "VX,VY=", self.get_velocityX() ,self.get_velocityY())
      end
    end --if (gameState.get_currentState() == SCREEN_PLAY) then 
  end

  --[[
  intercepte le clic de souris
    pX: position x de la souris, en pixels.
    pY: position y de la souris, en pixels.
    pButton: L'index du bouton qui a été pressé: 1 est le bouton principal, 2 est le bouton secondaire et 3 est le bouton du milieu.
    OPTIONNEL pIstouch: Vrai si le bouton est une touche tactile.
  ]]
  function self.mousepressed (pX, pY, pButton, pIstouch)
    --debugFunctionEnter("pawn.mousepressed:", pX, pY, pButton, pIstouch)
  end

  --[[
  incrémente le score du joueur
    OPTIONNEL pValue: valeur à ajouter
    RETURN: le score mis à jour
  ]]
  function self.addScore (pValue)
    if (pValue == nil) then pValue = 1 end
    score = score + pValue 
    return score
  end

  --[[
  décrémente la vie du joueur
    OPTIONNEL pValue: valeur à décrémenter
    RETURN: le nombre de vies mis à jour
  ]]
  function self.loseLife (pValue)
    livesLeft = livesLeft - pValue
    return livesLeft
  end

  --[[
  incrémente le niveau du joueur
    OPTIONNEL pValue: valeur à ajouter
    RETURN: le niveau mis à jour
  ]]
  function self.addLevel (pValue)
    if (pValue == nil) then pValue = 1 end
    level = level + pValue
    return level
  end 

  -- initialisation par défaut
  self.initialize()
  return self
end --function