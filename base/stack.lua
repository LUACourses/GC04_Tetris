-- Entité représentant une pile de valeurs 
-- Utilise une forme d'héritage simple: pile qui hérite de class
-- NOTE: contrairement à une héritage classique, les attributs de objets enfants sont COPIES depuis ceux du parent. 
-- donc CHAQUE CLASSE ENFANT DOIT UTILISER LES GETTERS/SETTERS DU PARENT POUR DÉFINIR LES ATTRIBUTS HÉRITÉS
--------------------------------

require("base/class")
require("base/helpers")

function newStack ()
  -- création d'un objet parent
  local self = class:new() 
  
  -- attributs privés accessibles via les getter/setters
  -------------  
  -- contenu
  local content = {}

  -- variables locales
  -------------

  --[[
  Initialise l'objet avec les valeurs par défaut
    OPTIONNEL pCount: Nombre de valeurs à créer
    OPTIONNEL pValue: Valeur à définir pour chaque élément. Si absent, la valeur d'initialisation sera 0.
  ]]
  function self.initialize (pCount, pValue)
    debugFunctionEnter("stack.initialize")
    content = {}
    each = iterator(content)
    if (pValue == nil) then pValue = 0 end
    if (pCount ~= nil and pCount > 0) then
      local i 
      for i = 1, pCount do
        self.push (pValue)
      end
    end
  end 

  -- getters et setters pour les attributs de cet objet
  --------------------------------
  function self.get ()
    return content
  end
  function self.set (pValue)
    content = pValue 
  end

  -- fonctions spécifiques
  --------------------------------
  -- Détruit l'objet
  function self.destroy ()
    -- debugFunctionEnter("stack.destroy")
    self.clean()
    self = nil
  end

  -- Effectue du nettoyage lié à l'objet en quittant le jeu 
  function self.clean ()
    -- debugFunctionEnter("stack.clean")
    -- pour le moment rien à faire 
  end
  
  --[[
  Ajoute une valeur à la fin de la pile
    pValue: valeur à ajouter
    RETURN: index de la valeur dans la pile
  ]]
  function self.push (pValue)
    --debugFunctionEnter("stack.push")
    local index = #content
    content[index + 1] = pValue
    return index
  end

  --[[
  Retourne la dernière valeur ajoutée
    pValue: valeur à ajouter
    RETURN: dernière valeur ajoutée ou nil si la pile est vide
  ]]
  function self.pop ()
    --debugFunctionEnter("stack.pop")
    if (#content < 1) then return nil end

    local index = #content
    return content[index]
  end

  --[[
  Retourne la valeur de l'élément donné
    pValue: index de l'élément
    RETURN: valeur de l'élément ou nil si l'index est invalide
  ]]
  function self.val (pIndex)
    --debugFunctionEnter("stack.add")
    if (content == nil) then
      -- cas ou la pile est vide et non initialisée
      self.initialize(1, pValue)
    end

    if (#content < pIndex) then return nil end
    return content[pIndex]
  end
  
  --[[
  Définit la valeur de tous les éléments de la pile
    pValue: valeur à définir
  ]]
  function self.reset (pValue)
    --debugFunctionEnter("stack.reset")
    if (#content < 1) then return end

    for element in self.each do
      element = pValue
    end
  end

  --[[
  Ajoute un élément
    pValue: valeur à ajouter
  ]]
  function self.add (pValue)
    --debugFunctionEnter("stack.add")
    if (#content < 1) then
      -- cas ou la pile est vide et non initialisée
      self.initialize(1, pValue)
    else 
      local i
      for i = 1, #content do
        content[i] = content[i] + pValue
      end
    end
  end

  --[[
  recherche une valeur et retourne la clé associée
    pValue: valeur à rechercher
    RETURN: clé de la valeur, nil si non trouvée
  ]]
  function self.find (pValue)
    --debugFunctionEnter("stack.find")
    if (#content < 1) then return nil end
    
    for key, valeur in pairs(content) do
      if (valeur == pValue) then return key end
    end
    return nil
  end

  --[[
  recherche une valeur et retourne l'index associé
    pValue: valeur à rechercher
    RETURN: index de la valeur, nil si non trouvée
  ]]
  function self.ifind (pValue)
    --debugFunctionEnter("stack.find")
    if (#content < 1) then return nil end
    
    for index, valeur in ipairs(content) do
      if (valeur == pValue) then return index end
    end
    return nil
  end

  -- initialisation par défaut
  self.initialize()
  return self

end --function