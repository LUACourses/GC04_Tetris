-- Entité représentant l'écran de jeu 
--------------------------------
require("base/helpers")

function newViewport ()
  local self = {}

  -- attributs privés accessibles via les getter/setters
  -------------

  -- variables locales
  -------------
  
  -- Initialise l'objet avec les valeurs par défaut
  function self.initialize ()
    debugFunctionEnter("viewport.initialize")

    -- position X minimale 
    self.Xmin = 0
    -- position X maximale 
    self.Xmax = love.graphics.getWidth()
    -- position Y minimale 
    self.Ymin = 0
    -- position Y maximale 
    self.Ymax = love.graphics.getHeight()
    -- largeur de caractère
    self.charWidth  = 10
    -- hauteur de caractère
    self.charHeight = 15
    -- couleur de fond par défaut
    self.backgroundColor = {0, 0, 50}
    -- couleur de texte par défaut
    self.textColor = {255, 255, 255}
    -- le viewport a t'il déjà été dessiné ?
    self.firstDrawDone = false
    
    -- cache le curseur de la souris
    love.mouse.setVisible(false)
    -- par défaut confine la souris dans la fenêtre
    love.mouse.setGrabbed(true)
  end

  -- Détruit l'objet
  function self.destroy ()
    debugFunctionEnter("viewport.destroy")
    self.clean()
    self = nil
  end

  -- Effectue des nettoyages en quittant le jeu 
  function self.clean ()
    debugFunctionEnter("viewport.clean")
    -- affiche le curseur de la souris
    love.mouse.setVisible(true)
  end
  
  -- Dessine l'objet
  function self.draw ()
    --debugFunctionEnter("viewport.draw") --ATTENTION cet appel peut remplir le log
    -- on utilise la couleur de fond définie pour remplir la zone de jeu
    love.graphics.setColor(self.backgroundColor[1], self.backgroundColor[2], self.backgroundColor[3])
    love.graphics.rectangle("fill", self.Xmin, self.Ymin, self.Xmax, self.Ymax)
    love.graphics.setColor(self.textColor[1], self.textColor[2], self.textColor[3])
    self.firstDrawDone = true
  end

  -- initialisation par défaut
  self.initialize()
  return self
end --function
