-- Entité représentant un objet fixe (non déplaçable)
-- Utilise une forme d'héritage simple: object qui hérite de class
-- NOTE: contrairement à une héritage classique, les attributs de objets enfants sont COPIES depuis ceux du parent. 
-- donc CHAQUE CLASSE ENFANT DOIT UTILISER LES GETTERS/SETTERS DU PARENT POUR DÉFINIR LES ATTRIBUTS HÉRITÉS
--------------------------------
require("base/helpers")
require("base/class")

function newObject ()
  -- création d'un objet parent
  local self = class:new() 
  
  -- attributs privés accessibles via les getter/setters
  -------------  
  -- position en X dans le viewport
  local x
  -- position en Y dans le viewport
  local y
  -- largeur
  local width
  -- hauteur
  local height

  -- variables locales
  -------------

  -- Initialise l'objet avec les valeurs par défaut
  function self.initialize ()
    debugFunctionEnter("object.initialize")
    assertEqualQuit(viewport, nil, "object.initialize:viewport", true)

    x      = 0
    y      = 0
    width  = 0
    height = 0
  end 

  -- getters et setters pour les attributs de cet objet
  --------------------------------
  function self.get_x ()
    return x
  end
  function self.set_x (pValue)
    x = pValue 
  end
  function self.get_y ()
    return y
  end
  function self.set_y (pValue)
    y = pValue 
  end
  function self.get_width ()
    return width
  end
  function self.set_width (pValue)
    width = pValue 
  end
  function self.get_height ()
    return height
  end
  function self.set_height (pValue)
    height = pValue 
  end

  -- fonctions spécifiques
  --------------------------------
  -- Détruit l'objet
  function self.destroy ()
    debugFunctionEnter("object.destroy")
    self.clean()
    self = nil
  end

  -- Effectue du nettoyage lié à l'objet en quittant le jeu 
  function self.clean ()
    debugFunctionEnter("object.clean")
    -- pour le moment rien à faire 
  end
  
  --[[
  Actualise l'objet (override)
    pDt: delta time
  ]]
  function self.update (pDt)
    --debugFunctionEnter("object.update") --ATTENTION cet appel peut remplir le log
    -- pour le moment rien à faire
  end

  -- Dessine l'objet
  function self.draw ()
    --debugFunctionEnter("object.draw") --ATTENTION cet appel peut remplir le log
    -- pour le moment rien à faire
  end

  -- initialisation par défaut
  self.initialize()
  return self
end --function