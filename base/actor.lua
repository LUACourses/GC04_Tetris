-- Entité représentant un objet pouvant se déplacer 
-- Utilise une forme d'héritage simple: actor qui hérite de object qui hérite de class
-- NOTE: contrairement à une héritage classique, les attributs de objets enfants sont COPIES depuis ceux du parent. 
-- donc CHAQUE CLASSE ENFANT DOIT UTILISER LES GETTERS/SETTERS DU PARENT POUR DÉFINIR LES ATTRIBUTS HÉRITÉS
--------------------------------

require("base/helpers")
require("base/object")

function newActor ()
  
  -- création d'un objet parent
  local super = newObject()
  -- création d'un objet enfant
  local self = super:new()
 
  -- attributs privés accessibles via les getter/setters
  -------------
  -- vitesse de départ
  local startSpeed
  -- vitesse de base
  local speed
  -- vitesse en X
  local velocityX
  -- vitesse en Y
  local velocityY
  -- vélocité maximale
  -- NOTE: si la valeur est nulle, l'objet ne pourra pas se déplacer
  local velocityMax
  -- poids (influence la gravité et la résistance de l'environnement)
  -- NOTE: si la valeur est nulle, l'objet ne sera pas soumis à la gravité et au freinage 
  local weight
  -- utilise les vélocités pour le déplacement de l'objet (mouvement automatique)
  -- NOTE: si true le mouvement utilisera l'inertie et le freinage, sinon les déplacements seront direct (déplacements standards)
  local moveWithVelocity
  -- modularité de la gravité
  -- NOTE: si la valeur est nulle, la gravité sera appliquée en continu (déplacement fluide), sinon elle sera appliquée avec la granularité définie (en pixel) 
  local gravityModularity
  -- timer pour gérer la granularité 
  local timerDrop

  -- variables locales
  -------------
  
  --[[
  Initialise l'objet avec les valeurs par défaut
    OPTIONNEL pStartSpeed: vitesse de départ
    OPTIONNEL pMoveWithVelocity: true pour utiliser les vélocités pour le déplacement de l'objet (mouvement automatique)
    OPTIONNEL pGravityModularity: modularité de la gravité 
  ]]
  function self.initialize (pStartSpeed, pMoveWithVelocity, pGravityModularity)
    debugFunctionEnter("actor.initialize")
    assertEqualQuit(viewport, nil, "actor.initialize:viewport", true)

    if (pStartSpeed == nil) then pStartSpeed = 0 end 
    if (pMoveWithVelocity == nil) then pMoveWithVelocity = true end 
    if (pGravityModularity == nil) then pGravityModularity = 0 end 

    -- attributs de la classe parent
    -------------
    super.set_x(0)
    super.set_y(0)
    super.set_width(0)
    super.set_height(0)

    -- attributs spécifiques à cette classe
    -------------
    startSpeed        = 0
    speed             = pStartSpeed
    velocityX         = 0
    velocityY         = 0
    velocityMax       = 6
    weight            = 1
    moveWithVelocity  = pMoveWithVelocity
    gravityModularity = pGravityModularity
    timerDrop         = 0
  end 

  -- getters et setters pour les attributs de cet objet
  --------------------------------
  function self.get_startSpeed ()
    return startSpeed
  end
  function self.set_startSpeed (pValue)
    startSpeed = pValue 
  end
  function self.get_speed ()
    return speed
  end
  function self.set_speed (pValue)
    speed = pValue 
  end
  function self.get_velocityX ()
    return velocityX
  end
  function self.set_velocityX (pValue)
    velocityX = pValue 
  end
  function self.get_velocityY ()
    return velocityY
  end
  function self.set_velocityY (pValue)
    velocityY = pValue 
  end
  function self.get_velocityMax ()
    return velocityMax
  end
  function self.set_weight (pValue)
    weight = pValue 
  end
  function self.get_weight ()
    return weight
  end
  function self.set_velocityMax (pValue)
    velocityMax = pValue 
  end
  function self.get_moveWithVelocity ()
    return moveWithVelocity
  end
  function self.set_moveWithVelocity (pValue)
    moveWithVelocity = pValue 
  end
  function self.get_gravityModularity ()
    return gravityModularity
  end
  function self.set_gravityModularity (pValue)
    gravityModularity = pValue 
  end
  function self.get_timerDrop ()
    return timerDrop
  end
  function self.set_timerDrop (pValue)
    timerDrop = pValue 
  end
  
  -- override des fonctions des parents
  --------------------------------
  -- Détruit l'objet (override)
  function self.destroy ()
    debugFunctionEnter("actor.destroy")
    self.clean()
    self = nil
  end

  -- Effectue du nettoyage lié à l'objet en quittant le jeu (override)
  function self.clean ()
    debugFunctionEnter("actor.clean")
    -- pour le moment rien à faire
  end

  --[[
  Actualise l'objet (override)
    pDt: delta time
  ]]
  function self.update (pDt)
    --debugFunctionEnter("actor.update") --ATTENTION cet appel peut remplir le log
    -- pour le moment rien à faire
    super.update(pDt)
  end

  -- Dessine l'objet (override)
  function self.draw ()
    --debugFunctionEnter("actor.draw") --ATTENTION cet appel peut remplir le log
    -- pour le moment rien à faire
  end

  -- fonctions spécifiques
  --------------------------------
  --[[
  Déplace l'objet
    pDt: delta time
  ]]
  function self.move (pDt) 
    --debugFunctionEnter("actor.move:", pDt) --ATTENTION cet appel peut remplir le log
    if (gameState.get_currentState() == SCREEN_PLAY) then 
      -- on tient compte de la gravité et du poids
      if (self.get_gravityModularity() <= 0) then
        -- le déplacement du à la gravité est fluide
        self.set_velocityY(self.get_velocityY() + (self.get_weight() * gameSettings.gravity * pDt))
      else 
        -- le déplacement du à la gravité se fait avec de la modularité
        self.set_timerDrop(self.get_timerDrop() + (self.get_weight() * gameSettings.gravity * pDt))
        if (self.get_timerDrop() >= self.get_gravityModularity()) then
          self.set_timerDrop(0)
          self.set_velocityY(self.get_velocityY() + self.get_gravityModularity())
        end
      end
      if (self.get_moveWithVelocity()) then
        -- Calcul des vélocités
        -----------------------
        -- on tient compte de la resistance de l'environnement (freinage) et du poids
        local dragWithWeightAndDt = self.get_weight() * gameSettings.velocityDrag * pDt
        if (self.get_velocityX() > 0) then 
          self.set_velocityX(self.get_velocityX() - (dragWithWeightAndDt))
        else 
          if (self.get_velocityX() < 0) then 
            self.set_velocityX(self.get_velocityX() + (dragWithWeightAndDt))
          end
        end
        if (self.get_velocityY() > 0) then 
          self.set_velocityY(self.get_velocityY() - (dragWithWeightAndDt))
        else
          if (self.get_velocityY() < 0) then 
            self.set_velocityY(self.get_velocityY() + (dragWithWeightAndDt))
          end
        end

        if (true) then
          -- on limite la vélocité 
          if (self.get_velocityX() < -self.get_velocityMax()) then 
            self.set_velocityX(-self.get_velocityMax())
          end 
          if (self.get_velocityY() < -self.get_velocityMax()) then 
            self.set_velocityY(-self.get_velocityMax())
          end  
          if (self.get_velocityX() > self.get_velocityMax()) then 
            self.set_velocityX(self.get_velocityMax())
          end 
          if (self.get_velocityY() > self.get_velocityMax()) then 
            self.set_velocityY(self.get_velocityMax())
          end  
        end
        -- Déplacements de l'objet
        -----------------------
        -- mouvement utilisant la vélocité et l'inertie
        super.set_x(super.get_x() + self.get_velocityX())
        super.set_y(super.get_y() + self.get_velocityY())
      else
        -- Déplacements de l'objet
        -----------------------
        -- mouvement standard
        super.set_x(super.get_x() + self.get_velocityX())
        super.set_y(super.get_y() + self.get_velocityY())
        self.set_velocityX(0)
        self.set_velocityY(0)
      end -- if (self.get_moveWithVelocity()) then
      --debugMessage("self. X="..math.floor(self.get_x())    .." Y="..math.floor(self.get_y())    .." VX="..self.get_velocityX().." VY="..self.get_velocityY())
    end --if (gameState.get_currentState() == SCREEN_PLAY) then 
  end --function

  --[[
  Vérifie les collisions et adapte le mouvement en conséquence
    OPTIONNEL pCollisionXmin: dimension X minimale à vérifier
    OPTIONNEL pCollisionXmax: dimension X maximale à vérifier
    OPTIONNEL pCollisionYmin: dimension Y minimale à vérifier
    OPTIONNEL pCollisionYmax: dimension Y maximale à vérifier
    RETURN : true si il y a une collision ou false sinon
    NOTE: en cas de collision, rectifie les attributs x et y de l'objet
  ]]
  function self.collide (pCollisionXmin, pCollisionXmax, pCollisionYmin, pCollisionYmax) 
    --debugFunctionEnter("actor.collide:", pCollisionXmin, pCollisionXmax, pCollisionYmin, pCollisionYmax) --ATTENTION cet appel peut remplir le log
    local isCollision = false
    assertEqualQuit(viewport, nil, "actor.collide:viewport", true)
    --debugMessage("actor.collide", pCollisionXmin, pCollisionXmax, pCollisionYmin, pCollisionYmax) 
    if (pCollisionXmin == nil) then pCollisionXmin = viewport.Xmin end
    if (pCollisionXmax == nil) then pCollisionXmax = (viewport.Xmax - super.get_width()) end
    if (pCollisionYmin == nil) then pCollisionYmin = (viewport.Ymin + super.get_height()) end
    if (pCollisionYmax == nil) then pCollisionYmax = (viewport.Ymax - super.get_height()) end

    -- on maintient l'objet dans l'écran
    if (super.get_x() < pCollisionXmin) then 
      super.set_x(pCollisionXmin)
      isCollision = true
    else
      if (super.get_x() > pCollisionXmax) then 
        super.set_x(pCollisionXmax) 
        isCollision = true
      end
    end

    if (super.get_y() < pCollisionYmin) then 
      super.set_y(pCollisionYmin)
      isCollision = true
    else
      if (super.get_y() > pCollisionYmax) then 
        super.set_y(pCollisionYmax) 
        isCollision = true
     end
    end 
    return isCollision
  end

  -- place l'acteur dans la zone de jeu
  function self.spawn ()
    debugFunctionEnter("actor.spawn")
    assertEqualQuit(viewport, nil, "actor.spawn:viewport", true)
    super.set_x(((viewport.Xmax - viewport.Xmin) - super.get_width()) / 2)
    super.set_y(((viewport.Ymax - viewport.Ymin) - super.get_height() - viewport.charHeight * 2) / 2)
  end  

  -- initialisation par défaut
  self.initialize()
  return self
end --function