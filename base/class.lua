-- Entité de base pour une classe
-- mise en place d'un principe de POO simplifié par clonage d'objet
-- NOTE: contrairement à une héritage classique, les attributs de objets enfants sont COPIES depuis ceux du parent. 
-- donc CHAQUE CLASSE ENFANT DOIT UTILISER LES GETTERS/SETTERS DU PARENT POUR DÉFINIR LES ATTRIBUTS HÉRITÉS
-------------------------
require("base/helpers")

--[[
clone un objet (i.e instancie une classe)
  OPTIONNEL pBaseObject: objet à cloner
  OPTIONNEL pCloneObject: objet cloné
  NOTE: l'usage le plus courant est 
    local pCloneObject = pBaseObject.new() 
]]
function new (pBaseObject, pCloneObject)
  if type(pBaseObject) ~= "table" then
    return pCloneObject or pBaseObject 
  end
  pCloneObject = pCloneObject or {}
  pCloneObject.__index = pBaseObject
  return setmetatable(pCloneObject, pCloneObject)
end

--[[
vérifie si un objet est d'une classe donnée
  OPTIONNEL pBaseObject: objet à cloner
  OPTIONNEL pCloneObject: objet cloné
  NOTE: l'usage le plus courant est 
      if (isA (pCloneObject,pBaseObject) then  
]]
function isA (pCloneObject, pBaseObject)
  local pClone_objeO_type = type(pCloneObject)
  local pBaseObject_type = type(pBaseObject)
  if pClone_objeO_type ~= "table" and pBaseObject_type ~= table then
    return pClone_objeO_type == pBaseObject_type
  end
  local index = pCloneObject.__index
  local _isa = index == pBaseObject
  while not _isa and index ~= nil do
    index = index.__index
    _isa = index == pBaseObject
  end
  return _isa
end

-- objet de base pour toutes les objets pouvant utiliser l'héritage
class = new(table, {new = new, isA = isA})