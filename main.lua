-- requires obligatoires pour le framework
require ("gameConstants")
require ("gameSettings")
require ("gameState")
require ("base/helpers")
require ("base/stack")
require ("base/viewport")
require ("base/musicManager")
require ("base/pawn")
require ("HUD")

-- requires spécifiques à l'application
require ("tetros")

-- Cette ligne permet d'afficher des traces dans la console pendant l'éxécution
io.stdout:setvbuf("no")

-- Empêche Love de filtrer les contours des images quand elles sont redimentionnées
-- Indispensable pour du pixel art
-- love.graphics.setDefaultFilter("nearest")

-- Cette ligne permet de déboguer pas à pas dans ZeroBraneStudio
if arg[#arg] == "-debug" then require("mobdebug").start() end

-- variables diverses
debugInfos = ""

-- Love.load pour initialiser l'application
function love.load ()
  --debugInfos = "Programme démarré...\n"
  love.window.setTitle(APP_TITLE) 
  local imgIcon = love.graphics.newImage(APP_ICON)
  love.window.setIcon(imgIcon:getData())
  -- Change les dimensions de la fenêtre
  love.window.setMode(APP_WINDOWS_WIDTH, APP_WINDOWS_HEIGHT) 

  -- Initialisation du random avec un temps en ms
  math.randomseed(love.timer.getTime()) 

  -- La répétition de l'événement love.keypressed est activés lorsqu'une touche est maintenue enfoncée.  
  love.keyboard.setKeyRepeat(true)
  
  -- Active le mode relatif: le curseur est caché et ne se déplace pas lorsque la souris est activée, 
  -- mais les événements relatifs du mouvement de la souris sont toujours générés via love.mousemoved
  love.mouse.setRelativeMode(true)

  initializeGame()
  startMenu()
end

--[[
Love.update est utilisé pour gérer l'état de l'application frame-to-frame
  pDt: delta time
]]
function love.update (pDt)
  tetros.update(pDt)

  player.update(pDt)
  HUD.update(pDt)

  --Actualise le contenu de l'écran
  if (gameState.get_currentState() == SCREEN_START) then
    updateStartScreen(pDt)
  elseif (gameState.get_currentState() == SCREEN_PLAY) then 
    updatePlayScreen(pDt)
  elseif (gameState.get_currentState() == SCREEN_PAUSE) then
    updatePauseScreen(pDt)
  elseif (gameState.get_currentState() == SCREEN_END) then 
    updateEndScreen(pDt)
  end

  --Actualise les musiques 
  musicManager.update(pDt)
end

 -- Love.draw est utilisé pour afficher l'état de l'application sur l'écran.
function love.draw ()
  -- si ce n'a pas déjà été fait,on dessine le viewport
  if (not viewport.firstDrawDone) then viewport.draw() end

  -- affiche l'écran de jeu
  if (gameState.get_currentState() == SCREEN_START) then
    HUD.drawStartScreen()
  elseif (gameState.get_currentState() == SCREEN_PLAY) then
    -- Affiche le joueur
    --player.draw()
    -- Dessine le tétromino courant
    tetros.draw()
    -- Dessine la grille
    tetros.drawGrid()
    -- Dessine la pile de tétromino à venir
    tetros.drawBag()

    HUD.drawPlayScreen()
  elseif (gameState.get_currentState() == SCREEN_PAUSE) then
    HUD.drawPauseScreen()
  elseif (gameState.get_currentState() == SCREEN_END) then 
    HUD.drawEndScreen()
  else 
    HUD.drawBadScreen()
  end 
end

-- intercepte l'appui des touches du clavier
function love.keypressed (pKey, pScancode, pIsrepeat)
  --debugMessage("touche clavier appuyée:", pKey, pScancode, pIsrepeat)

  -- évènements indépendants de l'écran
  ------------
  -- Quitter l'application
  if (pKey == gameSettings.gameKeys.quitGame) then quitGame() end
  -- Capturer ou non la souris dans la fenêtre
  if (pKey == gameSettings.gameKeys.grabMouse) then 
    local state = not love.mouse.isGrabbed()  
    love.mouse.setGrabbed(state)
    debugMessage("mouse.setGrabbed:", state) 
  end

  -- Jouer la piste musicale suivante
  if (pKey == gameSettings.gameKeys.nextMusic) then musicManager.playNext() end
  -- Jouer la piste musicale précédente
  if (pKey == gameSettings.gameKeys.previousMusic) then musicManager.playPrevious() end

  -- évènements liés à l'écran
  ------------
  if (gameState.get_currentState() == SCREEN_START) then
    keypressedStartScreen(pKey, pScancode, pIsrepeat)
  elseif (gameState.get_currentState() == SCREEN_PLAY) then 
    keypressedPlayScreen(pKey, pScancode, pIsrepeat)
  elseif (gameState.get_currentState() == SCREEN_PAUSE) then
    keypressedPauseScreen(pKey, pScancode, pIsrepeat)
  elseif (gameState.get_currentState() == SCREEN_END) then 
    keypressedEndScreen(pKey, pScancode, pIsrepeat)
  end
end

-- intercepte le relâchement des touches du clavier
function love.keyreleased (pKey, pScancode)
  -- si une touche figure dans la liste des touche à relacher, on mémorise que cette touche a été relâchée
  --if (player.get_keysToRelease() ~= nil and player.get_keyToRelease(pKey) ~= nil ) then player.set_keyToRelease(pKey, false) end
  if (tetros.get_keysToRelease() ~= nil and tetros.get_keyToRelease(pKey) ~= nil ) then tetros.set_keyToRelease(pKey, false) end
end

-- intercepte le mouvement de la souris
function love.mousemoved (pX, pY, pDX, pDY, pIstouch)
  --debugMessage("déplacement de la souris:", pX, pY, pDX, pDY, pIstouch)  --ATTENTION cet appel peut remplir le log
  
  -- évènements indépendants de l'écran
  ------------

  -- évènements liés à l'écran
  ------------
  if (gameState.get_currentState() == SCREEN_START) then
    mousemovedStartScreen(pX, pY, pDX, pDY, pIstouch)
  elseif (gameState.get_currentState() == SCREEN_PLAY) then 
    mousemovedPlayScreen(pX, pY, pDX, pDY, pIstouch)
  elseif (gameState.get_currentState() == SCREEN_PAUSE) then
    mousemovedPauseScreen(pX, pY, pDX, pDY, pIstouch)
  elseif (gameState.get_currentState() == SCREEN_END) then 
    mousemovedEndScreen(pX, pY, pDX, pDY, pIstouch)
  end
end
 
-- intercepte le clic de souris
function love.mousepressed (pX, pY, pButton, pIstouch)
  --debugMessage("boutons de la souris appuyé:",pX, pY, pButton, pIstouch)
  -- évènements indépendants de l'écran
  ------------
  
  -- évènements liés à l'écran
  ------------
  if (gameState.get_currentState() == SCREEN_START) then
    mousepressedStartScreen(pX, pY, pButton, pIstouch)
  elseif (gameState.get_currentState() == SCREEN_PLAY) then 
    mousepressedPlayScreen(pX, pY, pButton, pIstouch)
  elseif (gameState.get_currentState() == SCREEN_PAUSE) then
    mousepressedPauseScreen(pX, pY, pButton, pIstouch)
  elseif (gameState.get_currentState() == SCREEN_END) then 
    mousepressedEndScreen(pX, pY, pButton, pIstouch)
  end
end

-- Initialiser l'application
function initializeGame ()
  -- creation des entités
  -- Entité représentant les paramètres de l'application
  gameSettings = newGameSettings()
  -- Entité représentant l'état du jeu à un instant donné
  gameState    = newGameState()
  -- Entité représentant la zone de jeu 
  viewport     = newViewport()
   -- Entité représentant le music manager
  musicManager = newMusicManager() 
   -- Entité représentant l'affichage
  HUD          = newHUD() 
  -- Entité représentant le joueur
  player       = newPawn()

  -- Entité représentant les tetrominos
  tetros       = newTetros{}

  -- charge les musiques non gérées par le musicManager (menu , écran de fin)
  musicMenu = love.audio.newSource("musics/menu_Cool.mp3","stream")
  musicMenu:setLooping(true)
  musicMenu:setVolume(1)
  musicEnd = love.audio.newSource("musics/end_NodensField.mp3","stream")
  musicEnd:setLooping(true)
  musicEnd:setVolume(0.5)
  musicWin = love.audio.newSource("musics/win_Someday.mp3","stream")
  musicWin:setLooping(true)
  musicWin:setVolume(0.1)
  
  -- charge les musiques gérées par le musicManager
  musicManager.add("musics/tetris-gameboy-01.mp3","stream", 0.1)
  musicManager.add("musics/tetris-gameboy-02.mp3","stream", 0.1)
  musicManager.add("musics/tetris-gameboy-03.mp3","stream", 0.1)
  musicManager.add("musics/tetris-gameboy-04.mp3","stream", 0.1)
  musicManager.add("musics/tetris-gameboy-05.mp3","stream", 0.1)


  -- charge les sons du jeu
  sndNextLevel = love.audio.newSource("sounds/nextLevel.mp3","static")
  sndLoseLife = love.audio.newSource("sounds/loseLife.mp3","static")

  sndLineComplete = love.audio.newSource("sounds/lineComplete.mp3","static")

  -- paramètres spécifiques
  gameSettings.mouse.sensibilityX = 0.1
  gameSettings.mouse.sensibilityY = 0.1
end

-- Menu de démarrage 
function startMenu ()
  debugMessage("Menu de démarrage")
  gameState.initialize(20, 10) -- niveau 20 pour gagner, 10 lignes pour changer de niveau
  gameState.set_gameHasStarted(true)
  gameState.set_currentState(SCREEN_START)

  musicManager.stop() 
  musicEnd:stop()
  musicWin:stop()
  musicMenu:play()
end

-- Lancer l'application
function startGame ()
  debugMessage("Jeu démarré")
  if (not gameState.get_gameHasStarted()) then 
    -- cas ou on n'est pas passé par le startMenu
    gameState.initialize(20, 10) -- niveau 20 pour gagner, 10 lignes pour changer de niveau
  end 
  player.initialize(50, 50, 10, 5, 5, "", -1, 1, 0)
  gameState.set_currentState(SCREEN_PLAY)

  musicMenu:stop()
  musicEnd:stop()
  musicWin:stop()
  
  musicManager.play(1) 
  --player.spawn()

  tetros.initialize(0, 0, 0, 0, 3, "", -1, -1, -1, false, true, false) --3 est la valeur du "poids" et détermine la vitesse de descente du tetros

  -- Dessine la grille
  tetros.initGrid()
  tetros.drawGrid()
  tetros.spawn()
end

-- Mettre l'application en pause
function pauseGame()
  debugMessage("Pause")
  musicMenu:play()
  musicManager.pause()
  gameState.set_currentState(SCREEN_PAUSE)
end

-- Sort l'application du mode pause
function resumeGame()
  debugMessage("Resume")
  musicMenu:stop()
  musicManager.resume()
  gameState.set_currentState(SCREEN_PLAY)
end

-- le joueur perd une "vie"
function loseLife ()
  logMessage("Vie perdue")
  if (player.get_livesLeft() <= 1) then
    player.clean() -- juste au cas ou il y aurait une animation dans la destruction du joueur
    loseGame()
  else
    sndLoseLife.play()
    player.loseLife(1)
    player.spawn()
  end
end

--[[
le joueur passe au niveau suivant
  OPTIONNEL pNewLevel: valeur du nouveau niveau. Si absent, le niveau courant sera incrémenté de 1
]]
function nextLevel (pNewLevel)
  if (pNewLevel == nil) then 
    logMessage("Niveau suivant")
    player.addLevel(1)
  else
    logMessage("Niveau atteint:", pNewLevel)
    player.set_level(pNewLevel)
  end 
  
  if (player.get_level() >= gameState.get_levelToWin()) then
    -- l'application est terminée lorsque le niveau max est atteint
    winGame()
  else 
    sndNextLevel:play()
    musicManager.playNext()
    tetros.set_weight(tetros.get_weight() + 0.5) -- augmente la vitesse de descente
    --player.spawn()
  end
end 

-- le joueur perd la partie
function loseGame ()
  logMessage("Partie perdue")
  sndLoseLife:play()
  player.set_hasWon(false)
  musicEnd:play()
  musicManager.stop()
  gameState.set_currentState(SCREEN_END)
end

-- le joueur gagne la partie
function winGame ()
  logMessage("Partie gagnée")
  player.set_hasWon(true)
  musicWin:play()
  musicManager.stop()
  gameState.set_currentState(SCREEN_END)
end

-- Quitter l'application
function quitGame ()
  debugMessage("Quitter l'application")
  tetros.clean()
  player.clean()
  musicManager.clean()
  HUD.clean()
  gameState.clean()
  viewport.clean()
  love.event.push('quit') 
end

--[[
Actualise l'écran (menu) de départ
  pDt: delta time
]]
function updateStartScreen (pDt)
  --debugFunctionEnter("updateStartScreen") --ATTENTION cet appel peut remplir le log
  -- rien à faire pour le moment
  debugInfos = ""
end  

--[[
Actualise l'écran de pause
  pDt: delta time
]]
function updatePauseScreen (pDt)
  --debugFunctionEnter("updatePauseScreen") --ATTENTION cet appel peut remplir le log
  -- rien à faire pour le moment
  debugInfos = ""
end  

--[[
Actualise l'écran de fin
  pDt: delta time
]]
function updateEndScreen (pDt)
  --debugFunctionEnter("updateEndScreen") --ATTENTION cet appel peut remplir le log
  -- rien à faire pour le moment
  debugInfos = ""
end  

--[[
Actualise l'écran de jeu
  pDt: delta time
]]
function updatePlayScreen (pDt)
  --debugFunctionEnter("updatePlayScreen") --ATTENTION cet appel peut remplir le log
  -- déplacements et collisions du joueur  
  --player.move(pDt)
  --player.collide()
  -- déplacements et collisions des tétros  
  tetros.move(pDt)
  
  local collisionResult = tetros.collide()
  if (collisionResult == 1) then 
    tetros.restore() 
  elseif (collisionResult == 2) then 
    tetros.transfer()
  end
  --debugInfos = "joueur: X="..math.floor(player.get_x())    .." Y="..math.floor(player.get_y())    .." W="..player.get_width()    .." H="..player.get_height() 
  --debugInfos = debugInfos.."\n".."velocity : X="..math.floor(player.get_velocityX()).." Y="..math.floor(player.get_velocityY()).." speed="..math.floor(player.get_speed()) 
  debugInfos = "tetros: X="..math.floor(tetros.get_x())    .." Y="..math.floor(tetros.get_y())    .." shape="..tetros.get_currentShapeId()    .." rotation="..tetros.get_currentRotation() 
end

--[[
Gestion du "keypressed" sur l'écran (menu) de départ
...
]]
function keypressedStartScreen (pKey, pScancode, pIsrepeat)
  --debugFunctionEnter("keypressedStartScreen", pKey, pScancode, pIsrepeat)
  if (pKey ~= nil) then 
    startGame()    
  end 
end  

--[[
Gestion du "keypressed" sur l'écran de pause
...
]]
function keypressedPauseScreen (pKey, pScancode, pIsrepeat)
  --debugFunctionEnter("keypressedPauseScreen", pKey, pScancode, pIsrepeat)
  -- Sortir l'application du mode pause 
  if (pKey == gameSettings.gameKeys.pauseGame) then resumeGame() end    
end  

--[[
Gestion du "keypressed" sur l'écran de fin
...
]]
function keypressedEndScreen (pKey, pScancode, pIsrepeat)
  --debugFunctionEnter("keypressedEndScreen", pKey, pScancode, pIsrepeat)
  if (pKey == gameSettings.gameKeys.endGame) then quitGame() end    
  if (pKey == gameSettings.gameKeys.startGame) then startGame() end    
end  

--[[
Gestion du "keypressed" sur l'écran de jeu
...
]]
function keypressedPlayScreen (pKey, pScancode, pIsrepeat)
  --debugFunctionEnter("keypressedPlayScreen", pKey, pScancode, pIsrepeat)
  if (DEBUG_MODE) then 
    -- en mode debug , il existe une touche pour tuer la partie
    if (pKey == gameSettings.gameKeys.nextLevel) then nextLevel();return  end
    if (pKey == gameSettings.gameKeys.loseLife) then loseLife();return  end
    if (pKey == gameSettings.gameKeys.loseGame) then loseGame();return  end
    if (pKey == gameSettings.gameKeys.winGame) then winGame();return  end
  end

  if (pKey == gameSettings.gameKeys.pauseGame) then pauseGame() end

  -- NOTE: le deplacement du joueur avec les touches se fait dans player.move()
  --player.keypressed(pKey, pScancode, pIsrepeat)
  
  -- oblige le joueur à relacher des touches prédéfines àprès une action donnée 
  --if (player.get_keysToRelease() == nil or player.get_keyToRelease(pKey) == nil or player.get_keyToRelease(pKey) == false) then 
  --  player.keypressed(pKey, pScancode, pIsrepeat)
  --end 

  -- oblige le joueur à relacher la touche "down" à chaque nouveau tetros pour éviter qu'il ne tombe trop vite
  if (tetros.get_keysToRelease() == nil or tetros.get_keyToRelease(pKey) == nil or tetros.get_keyToRelease(pKey) == false) then 
    tetros.keypressed(pKey, pScancode, pIsrepeat)
  end 
end

--[[
Gestion du "mousemoved" sur l'écran (menu) de départ
...
]]
function mousemovedStartScreen (pX, pY, pDX, pDY, pIstouch)
  --debugFunctionEnter("mousemovedStartScreen",pX, pY, pDX, pDY, pIstouch) --ATTENTION cet appel peut remplir le log
  -- rien à faire pour le moment
end  

--[[
Gestion du "mousemoved" sur l'écran de pause
...
]]
function mousemovedPauseScreen (pX, pY, pDX, pDY, pIstouch)
  --debugFunctionEnter("mousemovedPauseScreen",pX, pY, pDX, pDY, pIstouch) --ATTENTION cet appel peut remplir le log
  -- rien à faire pour le moment
end  

--[[
Gestion du "mousemoved" sur l'écran de fin
...
]]
function mousemovedEndScreen (pX, pY, pDX, pDY, pIstouch)
  --debugFunctionEnter("mousemovedEndScreen",pX, pY, pDX, pDY, pIstouch) --ATTENTION cet appel peut remplir le log
  -- rien à faire pour le moment
end  

--[[
Gestion du "mousemoved" sur l'écran de jeu
...
]]
function mousemovedPlayScreen (pX, pY, pDX, pDY, pIstouch)
  --debugFunctionEnter("mousemovedPlayScreen",pX, pY, pDX, pDY, pIstouch) --ATTENTION cet appel peut remplir le log
  --player.mousemoved(pX, pY, pDX, pDY, pIstouch) 
  tetros.mousemoved(pX, pY, pDX, pDY, pIstouch)
end

--[[
Gestion du "mousepressed" sur l'écran (menu) de départ
...
]]
function mousepressedStartScreen (pX, pY, pButton, pIstouch)
  --debugFunctionEnter("mousepressedStartScreen")
  startGame()
end  

--[[
Gestion du "mousepressed" sur l'écran de pause
...
]]
function mousepressedPauseScreen (pX, pY, pButton, pIstouch)
  --debugFunctionEnter("mousepressedPauseScreen")
  -- rien à faire pour le moment
end  

--[[
Gestion du "mousepressed" sur l'écran de fin
...
]]
function mousepressedEndScreen (pX, pY, pButton, pIstouch)
  --debugFunctionEnter("mousepressedEndScreen")
  -- rien à faire pour le moment
end  

--[[
Gestion du "mousepressed" sur l'écran de jeu
...
]]
function mousepressedPlayScreen (pX, pY, pButton, pIstouch)
  --debugFunctionEnter("mousepressedPlayScreen")
  --player.mousepressed(pX, pY, pButton, pIstouch) 
  tetros.mousepressed(pX, pY, pButton, pIstouch)
end