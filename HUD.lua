-- Entité gérant l'affichage
--------------------------------
require("base/helpers")

-- positions relatives utilisées par la fonction displayText()
POS_TOP_LEFT      = "pos_top_left"
POS_TOP_RIGHT     = "pos_top_right"
POS_TOP_CENTER    = "pos_top_center"
POS_CENTER        = "pos_center"
POS_CENTER_LEFT   = "pos_center_left"
POS_CENTER_RIGHT  = "pos_center_right"
POS_BOTTOM_LEFT   = "pos_bottom_left"
POS_BOTTOM_RIGHT  = "pos_bottom_right"
POS_BOTTOM_CENTER = "pos_bottom_center"

-- animation possibles pour les textes utilisées par la fonction animText()
TEXT_ANIM_SINUS = "anim_sinus"

function newHUD ()
  local self = {}

  -- police pour les titres dans les menus
  local fontMenuTitle
  -- police pour le contenu dans les menus
  local fontMenuContent
  -- police pour les contenu (par défaut)
  local fontContent
  -- police pour les GUI (score, niveau...)
  local fontGUI
  -- police pour les affichages normaux (debug)
  local fontNormal
  -- différents timers pour gérer les animations utilisées par cet objet 
  local timerAnim
  -- dernier screen dessiné
  local lastDrawScreen
  --[[
  Initialise l'objet avec les valeurs par défaut
  ]]
  function self.initialize ()
    debugMessage("HUD.initialize")

    fontMenuTitle = love.graphics.newFont("fonts/menuTitle.ttf", 50)
    fontMenuContent = love.graphics.newFont("fonts/menuContent.ttf", 20)
    fontContent = love.graphics.newFont("fonts/content.ttf", 20)
    fontGUI = love.graphics.newFont("fonts/gui.ttf", 30)
    fontNormal = love.graphics.newFont("fonts/normal.ttf", 10)
    self.timerAnim = newStack()

    love.graphics.setFont(fontContent)
  end 
  
  -- Détruit l'objet
  function self.destroy ()
    debugFunctionEnter("HUD.destroy")
    self.clean()
    self = nil
  end

  -- Effectue du nettoyage lié à l'objet en quittant le jeu 
  function self.clean ()
    debugFunctionEnter("HUD.clean")
    -- pour le moment rien à faire
  end

  --[[
  Actualise l'objet (override)
    pDt: delta time
  ]]
  function self.update (pDt)
    --debugFunctionEnter("HUD.update") --ATTENTION cet appel peut remplir le log
    self.timerAnim.add(pDt)
  end
  
  -- Affiche l'écran d'erreur
  function self.drawBadScreen ()
    --debugFunctionEnter("HUD.drawBadScreen") --ATTENTION cet appel peut remplir le log
    self.displayText("MENU INVALIDE", fontContent, POS_CENTER, 0, 0, {255, 0, 0, 255}) --couleur rouge sans transparence
    lastDrawScreen = gameState.get_currentState()
  end  

  -- Affiche l'écran (menu) de départ
  function self.drawStartScreen ()
    --debugFunctionEnter("HUD.drawStartScreen") --ATTENTION cet appel peut remplir le log
    assertEqualQuit(viewport, nil, "HUD.drawStartScreen:viewport", true)
    assertEqualQuit(gameSettings, nil, "HUD.drawStartScreen:tetros", true)
   
    tetros.initGrid()
    tetros.drawGrid()    

    self.animText(APP_TITLE, fontMenuTitle, TEXT_ANIM_SINUS, 0, 50, tetros.get_colors())

    self.displayText("Appuyer sur une touche ou cliquer pour lancer le jeu", fontMenuContent, POS_CENTER, 0, 50, color)
    lastDrawScreen = gameState.get_currentState()
  end  

    -- Affiche l'écran de pause
  function self.drawPauseScreen ()
    --debugFunctionEnter("HUD.drawPauseScreen") --ATTENTION cet appel peut remplir le log

    tetros.drawGrid()    
    tetros.draw()

    self.displayText("PAUSE", fontMenuTitle, POS_CENTER, 0, 0, {255, 160, 0, 255}) --couleur orange sans transparence
    self.displayText('Appuyer sur "'..gameSettings.gameKeys.pauseGame..'" pour reprendre', fontMenuContent, POS_CENTER, 0, 50)
    lastDrawScreen = gameState.get_currentState()
  end  

    -- Affiche l'écran de fin
  function self.drawEndScreen ()
    --debugFunctionEnter("HUD.drawEndScreen") --ATTENTION cet appel peut remplir le log
    assertEqualQuit(gameSettings, nil, "HUD.drawEndScreen:tetros", true)

    if (player.get_hasWon() == true) then
      self.displayText("Bravo,partie gagnée !", fontMenuTitle, POS_CENTER, 0, 0, {0, 200, 0, 255})  --couleur verte sans transparence
      love.graphics.setColor(0, 0, 100, 128) -- bleu foncé 50%
    else  
      self.displayText("GAME OVER :o(", fontMenuTitle, POS_CENTER, 0, 0, {200, 0, 0, 255}) --couleur rouge sans transparence
      love.graphics.setColor(100, 0, 0, 128) -- rouge foncé 50%
    end 

    -- on ajoute un masque sur la grille 
    local grid = tetros.get_grid()
    love.graphics.rectangle("fill", grid.x, 0, grid.width * tetros.get_width(), grid.height * tetros.get_height(), 0, 0)
    
    self.displayText('Appuyer sur "'..gameSettings.gameKeys.quitGame ..'" pour quitter le jeu', fontMenuContent, POS_CENTER, 0, 50, {255, 160, 0, 255}) --couleur orange sans transparence
    self.displayText('Appuyer sur "'..gameSettings.gameKeys.startGame..'" pour rejouer', fontMenuContent, POS_CENTER, 0 , 80)

    self.displayText("Votre score est de "..player.get_score(), fontMenuContent, POS_CENTER, 0, 110)
    lastDrawScreen = gameState.get_currentState()
  end  

    -- Affiche l'écran de jeu
  function self.drawPlayScreen ()
    --debugFunctionEnter("HUD.drawPlayScreen") --ATTENTION cet appel peut remplir le log
    assertEqualQuit(gameSettings, nil, "HUD.drawPlayScreen:gameSettings", true)
    assertEqualQuit(gameState, nil, "HUD.drawPlayScreen:gameState", true)
    assertEqualQuit(viewport, nil, "HUD.drawPlayScreen:viewport", true)
    assertEqualQuit(player, nil, "HUD.drawPlayScreen:player", true)

    local charWidth = fontGUI:getWidth("A")
    local charHeight = fontGUI:getHeight("A")
    local drawX = (viewport.Xmax - viewport.Xmin)
    local drawY = (viewport.Ymax - viewport.Ymin)

    -- affiche le nombre de vie du joueur
    -- s'il est négatif, on affiche rien
    if (player.get_livesLeft() >= 0) then 
      local imgPath = player.get_livesImagePath()
      
      if (imgPath ~= "") then
        -- si le joueur a une image, on affiche l'image pour les vies restante
        love.graphics.setColor(255, 255, 255, 255) -- pour que les images soit visibles

        local i, livesImgWidth, livesImgHeight, livesImg
        livesImg = love.graphics.newImage(imgPath)
        livesImgWidth,livesImgHeight = livesImg:getDimensions()
        drawX = (viewport.Xmax - viewport.Xmin) - (livesImgWidth + 5) * player.get_livesStart() - 5
        drawY = 0
        for i = 0, player.get_livesLeft() - 1 do
          love.graphics.draw(livesImg, drawX + (livesImgWidth + 5) * i, drawY + 5, 0, 1, 1, 0, 0)
        end
      else
        -- sinon affiche un texte
        self.displayText("Vies "..player.get_livesLeft(), fontGUI, POS_TOP_RIGHT, -10, 5, {viewport.textColor[1], viewport.textColor[2], viewport.textColor[3], 255})
      end 
    end 

    -- affiche le score du joueur
    -- s'il est négatif, on affiche rien
    if (player.get_score() >= 0) then
      drawY = 5
      self.displayText("Score ", fontGUI, POS_TOP_LEFT, 10, drawY, {255, 160, 0, 255}) --couleur orange sans transparence
      self.displayText(player.get_score(), fontGUI, POS_TOP_LEFT, 10, drawY + charHeight, {255, 160, 0, 255}) --couleur orange sans transparence
    end

    -- affiche le nombre de lignes
    if (tetros.get_linesCount() >= 0) then
      drawY = drawY + charHeight * 3
      self.displayText("Lignes ", fontGUI, POS_TOP_LEFT, 10, drawY, {255, 160, 0, 255}) --couleur orange sans transparence
      self.displayText(tetros.get_linesCount(), fontGUI, POS_TOP_LEFT, 10, drawY + charHeight, {255, 160, 0, 255}) --couleur orange sans transparence
    end

    -- affiche le niveau du joueur
    -- s'il est négatif, on affiche rien
    if (player.get_level() >= 0) then 
      content = "Niveau "
      drawY = drawY + charHeight * 3
      local rectWidth = 12
      local rectHeight = 15
      self.displayText(content, fontGUI, POS_TOP_LEFT, 5, drawY, {255, 160, 0, 255}) --couleur orange sans transparence
      drawX = 5
      drawY = drawY + charHeight 
      for i = 0, gameState.get_levelToWin() - 1 do
        if (i < player.get_level()) then 
          love.graphics.setColor(255, 160, 0, 255) --couleur orange sans transparence
        else
          love.graphics.setColor(viewport.textColor[1], viewport.textColor[2], viewport.textColor[3], 128)
        end
        love.graphics.rectangle("fill", drawX + (i * rectWidth), drawY + 5 , rectWidth - 1, rectHeight , 2, 2)
      end
    end

    -- titre pour le liste des pièces
    if (#tetros.get_bag()>= 0) then
      self.displayText("A suivre", fontGUI, POS_TOP_RIGHT, -50, 10, {255, 160, 0, 255}) --couleur orange sans transparence
    end

    -- affichage des infos de debug (en bas de l'écran)
    if (DEBUG_MODE >= 2 and debugInfos ~= "") then
      self.displayText(debugInfos, fontNormal, POS_BOTTOM_LEFT, 5, -25, {0, 200, 0, 128}) -- vert foncé 50%
    end
    lastDrawScreen = gameState.get_currentState() 
  end  

  --[[
  Affiche un texte à l'écran en le positionnant relativement dans le viewport
    pText: texte à afficher
    pFont: police à utiliser
    OPTIONNEL pCONST_POSITON: position relative à l'écran (voir les constantes POS_xxx). Le texte sera centré à de l'écran par défaut
    OPTIONNEL pOffsetX: offset à ajouter en X 
    OPTIONNEL pOffsetY: offset à ajouter en Y 
    OPTIONNEL pColor: couleur du texte. La couleur blanc sera utilisée par défaut
  ]]
  function self.displayText (pText, pFont, pCONST_POSITON, pOffsetX, pOffsetY, pColor)
    assertEqualQuit(viewport, nil, "HUD.displayText:viewport", true)
    assertEqualQuit(pText, nil, "HUD.displayText:pText", true)
    assertEqualQuit(pFont, nil, "HUD.displayText:pFont", true)

    if (pCONST_POSITON == nil) then pCONST_POSITON = POS_CENTER end
    if (pOffsetX == nil) then pOffsetX = 0 end
    if (pOffsetY == nil) then pOffsetY = 0 end
    if (pColor == nil) then pColor = {{255, 255, 255, 255}} end

    love.graphics.setColor(pColor[1], pColor[2], pColor[3], pColor[4])
    
    love.graphics.setFont(pFont)

    local charWidth = pFont:getWidth(pText)
    local charHeight = pFont:getHeight(pText)
    local Xmax = (viewport.Xmax - viewport.Xmin - charWidth)
    local Ymax = (viewport.Ymax - viewport.Ymin - charHeight)
    switch (pCONST_POSITON) {
      [POS_TOP_LEFT]      = function () love.graphics.print(pText, pOffsetX            , pOffsetY           ) end,
      [POS_TOP_RIGHT]     = function () love.graphics.print(pText, pOffsetX + Xmax     , pOffsetY           ) end,
      [POS_TOP_CENTER]    = function () love.graphics.print(pText, pOffsetX + Xmax / 2 , pOffsetY           ) end,
      [POS_CENTER]        = function () love.graphics.print(pText, pOffsetX + Xmax / 2 , pOffsetY + Ymax / 2) end,
      [POS_CENTER_LEFT]   = function () love.graphics.print(pText, pOffsetX            , pOffsetY + Ymax / 2) end,
      [POS_CENTER_RIGHT]  = function () love.graphics.print(pText, pOffsetX + Xmax     , pOffsetY + Ymax / 2) end,
      [POS_BOTTOM_LEFT]   = function () love.graphics.print(pText, pOffsetX            , pOffsetY + Ymax    ) end,
      [POS_BOTTOM_RIGHT]  = function () love.graphics.print(pText, pOffsetX + Xmax     , pOffsetY + Ymax    ) end,
      [POS_BOTTOM_CENTER] = function () love.graphics.print(pText, pOffsetX + Xmax / 2 , pOffsetY + Ymax    ) end
    }
  end

  --[[
  Affiche un texte à l'écran en le centrant dans le viewport
    pText: texte à afficher
    pFont: police à utiliser
    OPTIONNEL pCONST_POSITON: position relative à l'écran (voir les constantes POS_xxx)
    OPTIONNEL pOffsetX: offset à ajouter en X 
    OPTIONNEL pOffsetY: offset à ajouter en Y 
    OPTIONNEL pColors: tableau contenant les couleur des lettres du texte. La couleur blanc sera utilisée par défaut 
    OPTIONNEL pTimerAnimIndex: index du timer gérant l'animation. 1 sera utilisé par défaut 
  ]]
  function self.animText (pText, pFont, pCONST_ANIM, pOffsetX, pOffsetY, pColors, pTimerAnimIndex)
    assertEqualQuit(viewport, nil, "HUD.displayText:viewport", true)
    assertEqualQuit(pText, nil, "HUD.displayText:pText", true)
    assertEqualQuit(pFont, nil, "HUD.displayText:pFont", true)

    if (pCONST_ANIM == nil) then pCONST_ANIM = TEXT_ANIM_SINUS end
    if (pOffsetX == nil) then pOffsetX = 0 end
    if (pOffsetY == nil) then pOffsetY = 0 end
    if (pColors == nil) then pColors = {{255, 255, 255, 255}} end
    if (pTimerAnimIndex == nil) then pTimerAnimIndex = 1 end
    
    love.graphics.setFont(pFont)

    local charWidth = pFont:getWidth(pText)
    local charHeight = pFont:getHeight(pText)
    local Xmax = (viewport.Xmax - viewport.Xmin - charWidth)
    local Ymax = (viewport.Ymax - viewport.Ymin - charHeight)
    local x = Xmax / 2
    local y = 0

    switch (pCONST_ANIM) {
      [TEXT_ANIM_SINUS] = function ()
        -- animation du texte avec une sinusoïdale
        local color, char
        local colorIndex = 1
        
        for c = 1, pText:len() do
          char = string.sub(pText, c, c)
          color = pColors[colorIndex]
          love.graphics.setColor(color[1], color[2], color[3])

          y = math.sin((x + self.timerAnim.val(pTimerAnimIndex) * 200) / 50) * 30
          love.graphics.print(char, x, y + ((viewport.Ymax - viewport.Ymin) - charHeight) / 2.5)
          x = x + fontMenuTitle:getWidth(char)
          colorIndex = colorIndex + 1
          if colorIndex > #pColors then colorIndex = 1 end
        end
      end
    }
  end

  -- initialisation par défaut
  self.initialize()
  return self
end --function