-- Entité représentant l'état du jeu à un instant donné
--------------------------------
require("base/helpers")

function newGameState ()
  local self = {}

  -- Etat courant de l'application
  local currentState
  -- le jeu a t'il démarré ?
  local gameHasStarted
  -- nombre de niveaux à atteindre pour gagner le jeu
  local levelToWin
  -- nombre de points à marquer pour passer un niveau
  local pointForLevel

  --[[
  Initialise l'objet avec les valeurs par défaut
    OPTIONNEL pLevelToWin: niveau à atteindre pour gagner le jeu
    OPTIONNEL pPointForLevel: nombre de points à marquer pour passer un niveau
  ]]
  function self.initialize (pLevelToWin, pPointForLevel)
    debugMessage("gameState.initialize:", pLevelToWin, pPointForLevel)
    if (pLevelToWin == nil) then pLevelToWin = 1 end
    if (pPointForLevel == nil) then pPointForLevel = 10 end

    currentState   = SCREEN_START
    gameHasStarted = false
    levelToWin     = pLevelToWin 
    pointForLevel  = pPointForLevel 
  end 
  
  -- getters et setters pour les attributs de cet objet
  --------------------------------
  function self.get_currentState ()
    return currentState
  end
  function self.set_currentState (pValue)
    currentState = pValue 
  end
  function self.get_gameHasStarted ()
    return gameHasStarted
  end
  function self.set_gameHasStarted (pValue)
    gameHasStarted = pValue 
  end
  function self.get_levelToWin ()
    return levelToWin
  end
  function self.set_levelToWin (pValue)
    levelToWin = pValue 
  end
  function self.get_pointForLevel ()
    return pointForLevel
  end
  function self.set_pointForLevel (pValue)
    pointForLevel = pValue 
  end

  -- Effectue du nettoyage lié à l'objet en quittant le jeu 
  function self.clean ()
    -- pour le moment rien à faire
  end

  -- initialisation par défaut
  self.initialize()
  return self
end --function